<?php

return array(
  'current_mode' => 'development',
  'connections' => array(
    'development' => array(
      "host"=>"localhost",
      "username"=>"root",
      "password"=>"",
      "database"=>"penjualan_cd_dvd"
    ),
    'production' => array(
      "host"=>"localhost",
      "username"=>"root",
      "password"=>"",
      "database"=>"penjualan_cd_dvd"
    )  
  ),
  'user_level_callback' => 'guest#login'
);