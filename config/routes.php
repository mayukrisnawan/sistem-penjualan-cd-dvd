<?php

Router::set("/", "guest#index");
Router::accept("guest");
Router::set("/save_payment_confirmation", "guest#save_payment_confirmation", array(
  "methods"=>"POST", 
  "name" => "save_payment_confirmation",
  "filters" => array("id" => "(\d+)")
));

Router::rest("types");
Router::rest("items", array(), array("item_tags"));
Router::set("/items/search", "items#search");

Router::rest("tags");
Router::set("/tags/json", "tags#json");

Router::rest("discounts");

Router::rest("suppliers");
Router::rest("purchases");

Router::rest("users", array(), array("user_roles"));
Router::set("/users/:id/reset_password", "users#reset_password", array(
  "methods"=>"PATCH", 
  "name" => "reset_password",
  "filters" => array("id" => "(\d+)")
));
Router::set("/users/change_password", "users#change_password_page", array(
  "methods"=>"GET", 
  "name" => "change_password_page"
));
Router::set("/users/change_password", "users#change_password", array(
  "methods"=>"POST", 
  "name" => "change_password"
));

Router::rest("roles", array(), array("role_application_features"));
Router::set("/role/:id/update_application_features", "roles#update_application_features", array(
  "methods"=>"PATCH", 
  "name" => "update_application_features_role",
  "filters" => array("id" => "(\d+)")
));

Router::accept("shopping_cart");
Router::set("/shopping_cart/:id/delete_item", "shopping_cart#delete_item", array(
  "methods"=>"DELETE", 
  "name" => "delete_shopping_cart_item",
  "filters" => array("id" => "(\d+)")
));

Router::rest("sales");
Router::set("/sales/payment_reply", "sales#payment_reply", array(
  "name" => "sale_payment_reply"
));
Router::set("/sales/update_payment_reply", "sales#update_payment_reply", array(
  "methods"=>"PATCH", 
  "name" => "update_payment_reply"
));
Router::set("/sale/:id/resend_payment_confirmation", "sales#resend_payment_confirmation", array(
  "methods"=>"POST", 
  "name" => "resend_payment_confirmation",
  "filters" => array("id" => "(\d+)")
));

Router::set("/sale/:id/agree", "sales#agree", array(
  "methods"=>"PATCH", 
  "name" => "agree_sale",
  "filters" => array("id" => "(\d+)")
));
Router::set("/sale/:id/disagree", "sales#disagree", array(
  "methods"=>"PATCH", 
  "name" => "disagree_sale",
  "filters" => array("id" => "(\d+)")
));
Router::set("/sale/:id/payment_invoice", "sales#payment_invoice", array(
  "methods"=>"GET", 
  "name" => "payment_invoice",
  "filters" => array("id" => "(\d+)")
));
