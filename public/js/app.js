function loadDotsAnimationInto(element, msg) {
  var useVal = false;
  if (!msg) {
    if ($(element).html()) {
      msg = $(element).html();
    } else {
      useVal = true;
      msg = $(element).val();
    }
  }
  var context = this;
  context.dots = 1;
  if (useVal) {
    $(element).val(msg);
  } else {
    $(element).html(msg);
  }

  timer = setInterval(function(){
    var dots = "";
    for (i=0; i<context.dots; i++) dots += ".";
    if (useVal) {
      $(element).val(msg+dots);
    } else {
      $(element).html(msg+dots);
    }
    context.dots += 1;
    context.dots %= 3;
  }, 700);
  return timer;
}

function stopDotsAnimationOn(timer, element, msg) {
  if (!timer) return false;
  clearTimeout(timer);
  if (msg) element.innerHTML = msg;
}


function ajaxSubmit(form, btnSubmit) {
  var data = "";
  var $form = $(form);
  var $btnSubmit = $(btnSubmit);
  $form.find("*[name]").each(function(key, element){
    var $element = $(element);
    if (!data == "") {
      data += "&";
    }
    data += $element.attr("name") + "=" + $element.val();
  });

  var target = $form.attr("action");
  if (!target) {
    target = $btnSubmit.attr("href");
    if (!target) return false;
  }
  var method = $form.attr("method");
  if (!method) {
    method = $btnSubmit.attr("method");
    if (!method) method = "GET";
  }

  var actionBackup = null;
  var timer = null;
  var contentBackup = null;
  var useVal = false;
  if ($btnSubmit.html()) {
    contentBackup = $btnSubmit.html();
    useVal = true;
  } else {
    contentBackup = $btnSubmit.val();
  }
  $.ajax({
    url:target,
    data:data,
    type:method,
    success:function(response){
      stopDotsAnimationOn(timer, btnSubmit, contentBackup);
      $btnSubmit.removeAttr("disabled");
      if (useVal) {
        $btnSubmit.html(contentBackup);
      } else {
        $btnSubmit.val(contentBackup);
      }
      $btnSubmit.get(0).onclick = actionBackup;
      eval(response);
    },
    beforeSend:function(){
      $btnSubmit.attr("disabled", "disabled");
      timer = loadDotsAnimationInto($btnSubmit);
      actionBackup = $btnSubmit.get(0).onclick;
    }
  });
  return false;
}

function alert(title, msg, callback) {
  if (title !== undefined && msg === undefined) {
    msg = title;
    title = $("title").html();
  }
  var modalId = '__alert';
  var modal = ["<div class='modal' id='"+modalId+"'>",
        "<div class='modal-dialog'>",
          "<div class='modal-content'>",
            "<div class='modal-header'>",
              "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>",
              "<h4 class='modal-title'><b>"+title+"</b></h4>",
            "</div>",
            "<div class='modal-body'>",
              "<p>"+msg+"</p>",
            "</div>",
            "<div class='modal-footer'>",
              "<button type='button' class='btn btn-success' data-dismiss='modal'>Kembali</button>",
            "</div>",
          "</div>",
        "</div>",
      "</div>"].join("");
  if ($("#"+modalId).length != 0) {
    $("#"+modalId).remove();
  }
  $("body").append(modal);
  $("#"+modalId).modal("show").on("hidden.bs.modal", function(){
    if (callback) callback();
  });
}

function confirm(title, msg, callback) {
  if (title !== undefined && msg === undefined) {
    msg = title;
    title = $("title").html();
  }
  var modalId = '__confirm';
  var modal = ["<div class='modal' id='"+modalId+"'>",
        "<div class='modal-dialog'>",
          "<div class='modal-content'>",
            "<div class='modal-header'>",
              "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>",
              "<h4 class='modal-title'><b>"+title+"</b></h4>",
            "</div>",
            "<div class='modal-body'>",
              "<p>"+msg+"</p>",
            "</div>",
            "<div class='modal-footer'>",
              "<button type='button' class='btn btn-success _btn_yes'>Ya</button>",
              "<button type='button' class='btn btn-danger _btn_no'>Tidak</button>",
            "</div>",
          "</div>",
        "</div>",
      "</div>"].join("");
  if ($("#"+modalId).length != 0) {
    $("#"+modalId).remove();
  }
  $("body").append(modal);
  $("#"+modalId).modal("show").on("hidden.bs.modal", function(){
    if (callback) callback();
  }).find("._btn_yes").click(function(){
    confirm.result = true;
    $("#"+modalId).modal("hide");
  });
   $("#"+modalId).find("._btn_no").click(function(){
    confirm.result = false;
    $("#"+modalId).modal("hide");
  });
}
confirm.result = false;


// create a list of <option> html tags
function createOption(data, contentKey, valueKey) {
  if (!valueKey) valueKey = "id";
  var result = "";
  for (var index in data) {
    result += "<option value='"+data[index][valueKey]+"'>"+data[index][contentKey]+"</option>";
  }
  return result;
}

$(document).ready(function(){
  // Remote links
  $("body").on("click", "[remote='true']", function(){
    var btnSubmit = this;
    if ($(this).attr("confirmTitle") && $(this).attr("confirmMessage")) {
      var title = $(this).attr("confirmTitle");
      var message = $(this).attr("confirmMessage");
      confirm(title, message, function(){
        if (confirm.result) {
          ajaxSubmit(null, btnSubmit);
        }
      });
      return false;
    } else {
      ajaxSubmit(null, btnSubmit);
      return false;
    }
  });
});

// datepicker
$(document).ready(function(){
  $('body').on('mouseenter', '.datepicker', function(){
    $this = $(this)
    $this.datepicker({
      format: 'dd-mm-yyyy'
    }).on("changeDate", function(){
      $this.datepicker('hide')
    })
  })
});