function selectItem(url, callback){
	$.ajax({
		url: url,
		success: function(response){
			$("#select_item_container").hide(0).html(response);
			$("#select_item_container").show(500);
			$(".btn-select-item").click(function(){
				var item_id = $(this).attr("data-id");
				var title = $(this).attr("data-title");
				var type = $(this).attr("data-type");

				var item = {
					id : item_id,
					title : title,
					type : type
				};
				callback(item);
				return false;
			});
		}
	});
}