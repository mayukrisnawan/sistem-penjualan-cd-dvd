/*
SQLyog Ultimate v9.20 
MySQL - 5.6.16 : Database - penjualan_cd_dvd
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`penjualan_cd_dvd` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `penjualan_cd_dvd`;

/*Table structure for table `application_features` */

DROP TABLE IF EXISTS `application_features`;

CREATE TABLE `application_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `application_features` */

insert  into `application_features`(`id`,`description`,`key`) values (1,'diskon','discounts'),(2,'barang','items'),(3,'kata kunci barang','item_tags'),(4,'member','members'),(5,'detail pembelian','purchase_items'),(6,'pembelian','purchases'),(7,'master jabatan pengguna','roles'),(8,'detail penjualan','sale_items'),(9,'penjualan','sales'),(10,'supplier','suppliers'),(11,'master kata kunci','tags'),(12,'tipe barang','types'),(13,'jabatan pengguna','user_roles'),(14,'pengguna','users'),(15,'hak akses pengguna','role_application_features');

/*Table structure for table `configs` */

DROP TABLE IF EXISTS `configs`;

CREATE TABLE `configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `configs` */

insert  into `configs`(`id`,`key`,`value`) values (1,'sale_payment_reply','Terima kasih telah bertransaksi di situs kami, berikut\nmerupakan detail transaksi dan pembayaran yang harus anda lakukan.\nPembayaran dilakukan melalui rekening berikut :\nBank Mandiri : 145-450-123-90\n');

/*Table structure for table `discounts` */

DROP TABLE IF EXISTS `discounts`;

CREATE TABLE `discounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `discounts` */

insert  into `discounts`(`id`,`start_date`,`finish_date`,`rate`,`item_id`) values (9,'2014-06-01','2014-06-30',0.12,14);

/*Table structure for table `item_tags` */

DROP TABLE IF EXISTS `item_tags`;

CREATE TABLE `item_tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `item_tags` */

insert  into `item_tags`(`id`,`item_id`,`tag_id`) values (4,10,2),(5,10,3),(6,11,2),(7,13,2),(8,14,3);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `items` */

insert  into `items`(`id`,`title`,`price`,`type_id`,`created_at`,`count`) values (14,'InsidiousInsidiousInsi',20000,2,'2014-06-27 13:20:49',49599),(15,'Insidious',50000,3,'2014-06-27 20:05:04',20);

/*Table structure for table `purchase_items` */

DROP TABLE IF EXISTS `purchase_items`;

CREATE TABLE `purchase_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_id` bigint(20) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_items` */

insert  into `purchase_items`(`id`,`purchase_id`,`price`,`count`,`item_id`) values (36,23,12000,45,14),(37,24,10000,50000,14),(38,24,20000,20,15);

/*Table structure for table `purchases` */

DROP TABLE IF EXISTS `purchases`;

CREATE TABLE `purchases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `at` datetime DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `purchases` */

insert  into `purchases`(`id`,`at`,`supplier_id`) values (23,'2014-06-27 00:00:00',4),(24,'2014-06-27 00:00:00',4);

/*Table structure for table `role_application_features` */

DROP TABLE IF EXISTS `role_application_features`;

CREATE TABLE `role_application_features` (
  `application_feature_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `c` tinyint(1) DEFAULT NULL,
  `r` tinyint(1) DEFAULT NULL,
  `u` tinyint(1) DEFAULT NULL,
  `d` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `role_application_features` */

insert  into `role_application_features`(`application_feature_id`,`role_id`,`c`,`r`,`u`,`d`) values (2,1,1,1,1,1),(5,1,1,1,1,1),(8,1,1,1,1,1),(1,1,1,1,1,1),(15,1,1,1,1,1),(13,1,1,1,1,1),(3,1,1,1,1,1),(7,1,1,1,1,1),(11,1,1,1,1,1),(4,1,1,1,1,1),(6,1,1,1,1,1),(14,1,1,1,1,1),(9,1,1,1,1,1),(10,1,1,1,1,1),(12,1,1,1,1,1),(2,2,1,1,1,1),(5,2,1,1,1,1),(8,2,0,0,0,0),(1,2,0,0,0,0),(15,2,0,0,0,0),(13,2,0,0,0,0),(3,2,0,0,0,0),(7,2,0,0,0,0),(11,2,0,0,0,0),(4,2,0,0,0,0),(6,2,0,0,0,0),(14,2,0,0,0,0),(9,2,0,0,0,0),(10,2,0,0,0,0),(12,2,0,0,0,0);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`) values (1,'admin'),(2,'operator');

/*Table structure for table `sale_items` */

DROP TABLE IF EXISTS `sale_items`;

CREATE TABLE `sale_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sale_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `discount_rate` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `sale_items` */

insert  into `sale_items`(`id`,`sale_id`,`item_id`,`count`,`price`,`discount_rate`) values (37,54,14,123,20000,0.12);

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `address` text,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT '0',
  `payment_confirmation_code` varchar(10) DEFAULT NULL,
  `payment_confirmation_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

/*Data for the table `sales` */

insert  into `sales`(`id`,`email`,`address`,`message`,`created_at`,`confirmed`,`payment_confirmation_code`,`payment_confirmation_image`) values (54,'mayu@ghjk.com','aghjkl;','adhajkldad','2014-07-01 02:37:30',0,'1234567891','rancangan database.png');

/*Table structure for table `suppliers` */

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `suppliers` */

insert  into `suppliers`(`id`,`name`,`phone`,`email`) values (2,'PT Kencana Entertainment','0879711131388','support@kencana.com'),(4,'PT Bintang Kejora','','');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tags` */

insert  into `tags`(`id`,`title`) values (2,'horor'),(3,'sci-fi');

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `types` */

insert  into `types`(`id`,`name`) values (2,'CD'),(3,'DVD');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`user_id`,`role_id`) values (7,2,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`) values (2,'admin','21232f297a57a5a743894a0e4a801fc3');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
