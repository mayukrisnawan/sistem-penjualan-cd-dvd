<?php

function cek_sudah_login_sebagai_admin() {
  if (!UM::isLogin()) {
    header("Location:". u("guest#login"));
    exit();
  }
}

function cek_punya_akses_untuk_mata_kuliah($id) {
  UM::requiredLevels(array("admin", "dosen"));
  $user = UM::currentUser();
  $lecturer = $user->lecturer;
  if ($user->level == UM::levelId("dosen")) {
    try {
      $subject = Subject::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    if ($subject->lecturer_id != $lecturer->id) {
      header("Location:". u("guest#login"));
      exit();
    }
  }
}

function getPlainText($html, $numchars) {
  // Remove the HTML tags
  $html = strip_tags($html);

  // Convert HTML entities to single characters
  $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');

  // Make the string the desired number of characters
  // Note that substr is not good as it counts by bytes and not characters
  $html = mb_substr($html, 0, $numchars, 'UTF-8');

  // Add an elipsis
  $html .= "…";

  return $html;
}

function sendPDF($content, $filename) {
  require_once "libs/html2pdf/html2pdf.class.php";
  set_time_limit(10000);
  ini_set("memory_limit", "200M");
  $html2pdf = new HTML2PDF('L', 'A4', 'en', 'true', 'UTF-8', array(15,15,15,15));
  $html2pdf->setDefaultFont("Calibri");
  $html2pdf->writeHTML($content);
  $html2pdf->Output('$filename.pdf');
}