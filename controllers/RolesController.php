<?php

class RolesController extends Controller {
  function __construct() {
    parent::__construct();
  }

  public function update_application_features($id) {
    restrict("update", "role_application_features");
    try {
      $this->data->role = Role::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    RoleApplicationFeature::delete_all(array(
      "conditions"=>array("role_id" => $id)
    ));
    $this->data->role->setApplicationFeatures(params("application_features"));
    $this->render("roles/update_application_features");
  }

  public function index() {
    restrict("read", "roles");
    $this->data->pagination = Role::paginate();
    $this->render("roles/index");
  }

  public function add() {
    restrict("create", "roles");
    $this->data->action = u("roles#create"); 
    $this->data->method = "post"; 
    $this->render("roles/add");
  }

  public function create() {
    restrict("create", "roles");
    $this->data->role = new Role();
    foreach (params("role") as $key => $value) {
      $this->data->role->$key = $value;
    }
    if ($this->data->role->save()) {
      $this->render("roles/create_success");
    } else {
      $this->data->errors = $this->data->role->errors->fetch();
      $this->render("roles/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "roles");
    try {
      $this->data->role = Role::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("roles#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("roles/edit");
  }

  public function update($id) {
    restrict("update", "roles");
    try {
      $this->data->role = Role::find($id);
    } catch (Exception $e) {
      $this->render("roles/update_success");
    }
    foreach (params("role") as $key => $value) {
      $this->data->role->$key = $value;
    }
    if ($this->data->role->save()) {
      $this->render("roles/update_success");
    } else {
      $this->data->errors = $this->data->role->errors->fetch();
      $this->render("roles/update_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "roles");
    try {
      $role = Role::find($id);
    } catch (Exception $e) {
      $this->render("roles/destroy_success");
    }
    if ($role->delete()) {
      $this->render("roles/destroy_success");
    } else {
      $this->render("roles/destroy_errors");
    }
  }
}