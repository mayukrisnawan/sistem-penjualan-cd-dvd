<?php

class ItemTagsController extends Controller {
  function __construct() {
    parent::__construct();
  }

  function findItem($id){
    try {
      $this->data->item = Item::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
  }

  public function index($item_id) {
    restrict("read", "item_tags");
    $this->findItem($item_id);
    // var_dump($this->data->item->tags);
    $this->data->item_tags = $this->data->item->item_tags;
    $this->render("items/tags/index");
  }

  public function add($item_id) {
    restrict("create", "item_tags");
    $this->findItem($item_id);
    $this->data->action = u("item_item_tags#create", array("item_id"=>$item_id)); 
    $this->data->method = "post"; 
    $this->render("items/tags/add");
  }

  public function create($item_id) {
    restrict("create", "item_tags");
    $this->findItem($item_id);
    $this->data->item_tag = ItemTag::create(params("item_tag"));

    if ($this->data->item_tag->save()) {
      $this->render("items/tags/create_success");
    } else {
      $this->data->errors = $this->data->item_tag->errors->fetch();
      $this->render("items/tags/create_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "item_tags");
    try {
      $item_tag = ItemTag::find($id);
    } catch (Exception $e) {
      $this->render("items/tags/destroy_success");
    }
    if ($item_tag->delete()) {
      $this->render("items/tags/destroy_success");
    } else {
      $this->render("items/tags/destroy_errors");
    }
  }
}