<?php

class SalesController extends Controller {
  public function create() {
    // restrict("create", "sales");
    $this->data->sale = new Sale();
    foreach (params("sale") as $key => $value) {
      $this->data->sale->$key = $value;
    }
    if ($this->data->sale->save()) {
      $cart_items = ShoppingCart::all();

      foreach ($cart_items as $cart_item) {
        $sale_item = new SaleItem();
        // var_dump($sale_item);
        $sale_item->sale_id = $this->data->sale->id;
        $sale_item->item_id = $cart_item["info"]->id;
        $sale_item->count = $cart_item["count"];
        $sale_item->price = $cart_item["info"]->price;
        $sale_item->discount_rate = $cart_item["info"]->discount_rate();
        $sale_item->save();
      }

      // ONLINE
      if ($this->data->sale->send_confirmation()) {
        ShoppingCart::flush();
        $this->render("sales/create_success");
      } else {
        $this->data->sale->delete();
        $this->render("sales/create_errors");
      }
    } else {
      $this->data->errors = $this->data->sale->errors->fetch();
      $this->render("sales/create_errors");
    }
  }

  public function payment_reply() {
    restrict("manage", "sales");
    $this->data->message = Config::get("sale_payment_reply");
    $this->render("sales/payment_reply");
  }

  public function update_payment_reply() {
    restrict("manage", "sales");
    if ( Config::set("sale_payment_reply", params("message")) ) {
      $this->render("sales/update_payment_reply_success");
    } else {
      $this->render("sales/update_payment_reply_error");
    }
  }

  public function index() {
    restrict("read", "sales");
    $this->data->pagination = Sale::paginate(array(
      "order"=> "confirmed ASC, created_at DESC"
    ));
    $this->render("sales/index");
  }

  public function agree($id) {
    restrict("manage", "sales");
    try {
      $sale = Sale::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $sale->confirmed = 1;
    $sale->reduce_items_count();
    $sale->save();
    $this->render("sales/refresh");
  }

  public function disagree($id) {
    restrict("manage", "sales");
    try {
      $sale = Sale::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $sale->confirmed = 2;
    $sale->save();
    $this->render("sales/refresh");
  }

  public function resend_payment_confirmation($id) {
    restrict("manage", "sales");
    try {
      $sale = Sale::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    if ($sale->send_confirmation()) {
      $this->render("sales/send_confirmation_success");
    } else {
      $this->render("sales/send_confirmation_failed");
    }
  }

  public function payment_invoice($id) {
    restrict("manage", "sales");
    try {
      $this->data->sale = Sale::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->render("sales/payment_invoice");
  }
}