<?php

class SuppliersController extends Controller {
  public function index() {
    restrict("read", "suppliers");
    $this->data->pagination = Supplier::paginate();
    $this->render("suppliers/index");
  }

  public function add() {
    restrict("create", "suppliers");
    $this->data->action = u("suppliers#create"); 
    $this->data->method = "post"; 
    $this->render("suppliers/add");
  }

  public function create() {
    restrict("create", "suppliers");
    $this->data->supplier = new Supplier();
    foreach (params("supplier") as $key => $value) {
      $this->data->supplier->$key = $value;
    }
    if ($this->data->supplier->save()) {
      $this->render("suppliers/create_success");
    } else {
      $this->data->errors = $this->data->supplier->errors->fetch();
      $this->render("suppliers/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "suppliers");
    try {
      $this->data->supplier = Supplier::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("suppliers#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("suppliers/edit");
  }

  public function update($id) {
    restrict("update", "suppliers");
    try {
      $this->data->supplier = Supplier::find($id);
    } catch (Exception $e) {
      $this->render("suppliers/update_success");
    }
    foreach (params("supplier") as $key => $value) {
      $this->data->supplier->$key = $value;
    }
    if ($this->data->supplier->save()) {
      $this->render("suppliers/update_success");
    } else {
      $this->data->errors = $this->data->supplier->errors->fetch();
      $this->render("suppliers/update_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "suppliers");
    try {
      $supplier = Supplier::find($id);
    } catch (Exception $e) {
      $this->render("suppliers/destroy_success");
    }
    if ($supplier->delete()) {
      $this->render("suppliers/destroy_success");
    } else {
      $this->render("suppliers/destroy_errors");
    }
  }
}