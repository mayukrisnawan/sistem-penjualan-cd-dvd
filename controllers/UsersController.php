<?php

class UsersController extends Controller {
  public function reset_password($id) {
    restrict("update", "users");
    $user = User::find($id);
    $user->password = md5($user->username);
    $user->save();
    $this->render("users/reset_password");
  }

  public function index() {
    restrict("read", "users");
    $this->data->pagination = User::paginate();
    $this->render("users/index");
  }

  public function add(){
    restrict("create", "users");
  	$this->data->action = u("users#create"); 
    $this->data->method = "post"; 
    $this->render("users/add");
  }

  public function create() {
    restrict("create", "users");
    $this->data->user = new User();
    $this->data->user->username = params("user")["username"];
    $this->data->user->password = md5(params("user")["username"]);
    if ($this->data->user->save()) {
      $this->render("users/create_success");
    } else {
      $this->data->errors = $this->data->user->errors->fetch();
      $this->render("users/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "users");
    try {
      $this->data->user = User::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("users#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("users/edit");
  }

  public function update($id) {
    restrict("update", "users");
    try {
      $this->data->user = User::find($id);
    } catch (Exception $e) {
      $this->render("users/update_success");
    }
    foreach (params("user") as $key => $value) {
      $this->data->user->$key = $value;
    }
    if ($this->data->user->save()) {
      $this->render("users/update_success");
    } else {
      $this->data->errors = $this->data->user->errors->fetch();
      $this->render("users/update_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "users");
    try {
      $user = User::find($id);
    } catch (Exception $e) {
      $this->render("users/destroy_success");
    }
    if ($user->delete()) {
      $this->render("users/destroy_success");
    } else {
      $this->render("users/destroy_errors");
    }
  }

  public function change_password_page() {
    if (current_user()) {
      $this->render("users/change_password_page");
    } else {
      App::notFound();
    }
  }

  public function change_password() {
    if (!params("old_password")) {
      $this->data->error = "Password lama belum diisi";
    } else if (!params("new_password")) {
      $this->data->error = "Password baru belum diisi";
    } else if (params("new_password") != params("new_password_retype")) {
      $this->data->error = "Password yang diketik ulang tidak sesuai dengan password baru";
    }

    $user = current_user();
    if ($user->password != md5(params("old_password"))) {
      $this->data->error = "Password lama tidak valid";
    } else {
      $user->password = md5(params("new_password"));
      if (!$user->save()) {
        $this->data->error = "Terjadi kesalahan password tidak berhasil dirubah";
      }
    }
    $this->render("users/change_password");
  }
}