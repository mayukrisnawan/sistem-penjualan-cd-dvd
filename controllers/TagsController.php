<?php

class TagsController extends Controller{
  public function json() {
    restrict("read", "tags");
    $content = params("content");
    if (!$content) exit();
    $tags = Tag::all(
      array(
        "conditions"=>array("title LIKE ?", "%".$content."%"),
        "order"=>"title"
      )
    );
    echo Model::json($tags);
  }

  public function index() {
    restrict("read", "tags");
    $this->data->pagination = Tag::paginate(array(
      "order" => "title"
    ));
    $this->render("tags/index");
  }

  public function add() {
    restrict("create", "tags");
    $this->data->action = u("tags#create"); 
    $this->data->method = "post"; 
    $this->render("tags/add");
  }

  public function create() {
    restrict("create", "tags");
    $this->data->tag = new Tag();
    foreach (params("tag") as $key => $value) {
      $this->data->tag->$key = $value;
    }
    if ($this->data->tag->save()) {
      $this->render("tags/create_success");
    } else {
      $this->data->errors = $this->data->tag->errors->fetch();
      $this->render("tags/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "tags");
    try {
      $this->data->tag = Tag::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("tags#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("tags/edit");
  }

  public function update($id) {
    restrict("update", "tags");
    try {
      $this->data->tag = Tag::find($id);
    } catch (Exception $e) {
      $this->render("tags/update_success");
    }
    foreach (params("tag") as $key => $value) {
      $this->data->tag->$key = $value;
    }
    if ($this->data->tag->save()) {
      $this->render("tags/update_success");
    } else {
      $this->data->errors = $this->data->tag->errors->fetch();
      $this->render("tags/update_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "tags");
    try {
      $tag = Tag::find($id);
    } catch (Exception $e) {
      $this->render("tags/destroy_success");
    }
    if ($tag->delete()) {
      $this->render("tags/destroy_success");
    } else {
      $this->render("tags/destroy_errors");
    }
  }
}