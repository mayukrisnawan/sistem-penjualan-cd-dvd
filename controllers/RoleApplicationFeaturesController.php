<?php

class RoleApplicationFeaturesController extends Controller {
  function findRole($role_id) {
    try {
      $this->data->role = Role::find($role_id);
    } catch (Exception $e) {
      App::notFound();
    }
  }

  public function index($role_id) {
    restrict("read", "role_application_features");
    $this->findRole($role_id);
    $this->data->role_application_features = $this->data->role->application_features;
    $this->data->application_features = ApplicationFeature::all(array(
      "order" => "description"
    ));
    $this->render("roles/application_features/index");
  }
}