<?php

class ShoppingCartController extends Controller {
  public function add() {
  	ShoppingCart::set(params("item_id"), params("count"));
    $this->render("shopping_cart/refresh");
  }

  public function clear() {
  	ShoppingCart::flush();
    $this->render("shopping_cart/refresh");
  }

  public function show() {
    $this->data->cart_items = ShoppingCart::all();
  	$this->render("shopping_cart/show");
  }

  public function delete_item($item_id) {
    ShoppingCart::deleteItem($item_id);
    $this->render("shopping_cart/refresh");
  }

  public function save() {
    $this->data->action = u("sales#create");
    $this->data->method = "post";
    $this->render("shopping_cart/save");
  }
}