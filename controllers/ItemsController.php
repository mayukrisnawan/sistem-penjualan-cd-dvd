<?php

class ItemsController extends Controller {
  public function search() {
    restrict("read", "items");
    if (params("query") and params("query") != "") {
      $this->data->items = Item::search(params("query"));
    } else {
      $this->data->items = array();
    }
    $this->render("items/search");
  }

  public function index() {
    restrict("read", "items");
    $this->data->pagination = Item::paginate();
    $this->render("items/index");
  }

  public function add() {
    restrict("create", "items");
    $this->data->action = u("items#create"); 
    $this->data->method = "post"; 
    $this->data->types = Type::all();
    $this->render("items/add");
  }

  public function create() {
    restrict("create", "items");
    $this->data->item = new Item();
    foreach (params("item") as $key => $value) {
      $this->data->item->$key = $value;
    }
    if ($this->data->item->save()) {
      $this->render("items/create_success");
    } else {
      $this->data->errors = $this->data->item->errors->fetch();
      $this->render("items/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "items");
    try {
      $this->data->item = Item::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->types = Type::all();
    $this->data->action = u("items#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("items/edit");
  }

  public function update($id) {
    restrict("update", "items");
    try {
      $this->data->item = Item::find($id);
    } catch (Exception $e) {
      $this->render("items/update_success");
    }
    foreach (params("item") as $key => $value) {
      $this->data->item->$key = $value;
    }
    if ($this->data->item->save()) {
      $this->render("items/update_success");
    } else {
      $this->data->errors = $this->data->item->errors->fetch();
      $this->render("items/update_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "items");
    try {
      $item = Item::find($id);
    } catch (Exception $e) {
      $this->render("items/destroy_success");
    }
    if ($item->delete()) {
      $this->render("items/destroy_success");
    } else {
      $this->render("items/destroy_errors");
    }
  }
}