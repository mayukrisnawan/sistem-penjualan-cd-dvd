<?php

class DiscountsController extends Controller {
  public function index() {
    restrict("read", "discounts");
    $this->data->pagination = Discount::paginate(array(
      "order" => "start_date desc"
    ));
    $this->render("discounts/index");
  }

  public function add() {
    restrict("create", "discounts");
    $this->data->action = u("discounts#create"); 
    $this->data->method = "post"; 
    $this->render("discounts/add");
  }

  public function create() {
    restrict("create", "discounts");
    $this->data->discount = new Discount();
    foreach (params("discount") as $key => $value) {
      $this->data->discount->$key = $key == 'rate' ? $value/100 : $value;
    }
    if ($this->data->discount->save()) {
      $this->render("discounts/create_success");
    } else {
      $this->data->errors = $this->data->discount->errors->fetch();
      $this->render("discounts/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "discounts");
    try {
      $this->data->discount = Discount::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("discounts#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("discounts/edit");
  }

  public function update($id) {
    restrict("update", "discounts");
    try {
      $this->data->discount = Discount::find($id);
    } catch (Exception $e) {
      $this->render("discounts/update_success");
    }
    foreach (params("discount") as $key => $value) {
      $this->data->discount->$key = $key == 'rate' ? $value/100 : $value;
    }
    if ($this->data->discount->save()) {
      $this->render("discounts/update_success");
    } else {
      $this->data->errors = $this->data->discount->errors->fetch();
      $this->render("types/update_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "discounts");
    try {
      $discount = Discount::find($id);
    } catch (Exception $e) {
      $this->render("discounts/destroy_success");
    }
    if ($discount->delete()) {
      $this->render("discounts/destroy_success");
    } else {
      $this->render("discounts/destroy_errors");
    }
  }
}