<?php

class TypesController extends Controller {
  public function index() {
    restrict("read", "types");
    $this->data->pagination = Type::paginate();
    $this->render("types/index");
  }

  public function add() {
    restrict("create", "types");
    $this->data->action = u("types#create"); 
    $this->data->method = "post"; 
    $this->render("types/add");
  }

  public function create() {
    restrict("create", "types");
    $this->data->type = new Type();
    foreach (params("type") as $key => $value) {
      $this->data->type->$key = $value;
    }
    if ($this->data->type->save()) {
      $this->render("types/create_success");
    } else {
      $this->data->errors = $this->data->type->errors->fetch();
      $this->render("types/create_errors");
    }
  }

  public function edit($id) {
    restrict("update", "types");
    try {
      $this->data->type = Type::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("types#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("types/edit");
  }

  public function update($id) {
    restrict("update", "types");
    try {
      $this->data->type = Type::find($id);
    } catch (Exception $e) {
      $this->render("types/update_success");
    }
    foreach (params("type") as $key => $value) {
      $this->data->type->$key = $value;
    }
    if ($this->data->type->save()) {
      $this->render("types/update_success");
    } else {
      $this->data->errors = $this->data->type->errors->fetch();
      $this->render("types/update_errors");
    }
  }

  public function destroy($id) {
    restrict("destroy", "types");
    try {
      $type = Type::find($id);
    } catch (Exception $e) {
      $this->render("types/destroy_success");
    }
    if ($type->delete()) {
      $this->render("types/destroy_success");
    } else {
      $this->render("types/destroy_errors");
    }
  }
}