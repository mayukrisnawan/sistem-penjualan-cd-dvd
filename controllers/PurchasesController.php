<?php

class PurchasesController extends Controller {
  public function index() {
    restrict("read", "purchases");
    $this->data->pagination = Purchase::paginate(array(
      "order" => "at DESC"
    ));
    $this->render("purchases/index");
  }

  public function show($id) {
    restrict("read", "purchases");
    try {
      $this->data->purchase = Purchase::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->render("purchases/show");
  }

  public function add() {
    restrict("create", "purchases");
    $this->data->suppliers = Supplier::all(array("order" => "name"));
    $this->data->action = u("purchases#create"); 
    $this->data->method = "post"; 
    $this->data->types = Type::all();
    $this->render("purchases/add");
  }

  public function create() {
    restrict("create", "purchases");
    $this->data->purchase = new Purchase();
    foreach (params("purchase") as $key => $value) {
      $this->data->purchase->$key = $value;
    }
    $success = $this->data->purchase->save();

    if (params("purchase_item_count") && params("purchase_item_price")) {
      foreach (params("purchase_item_count") as $item_id => $count) {
        // $item = Item::find($item_id);
        $pi = new PurchaseItem();
        $pi->purchase_id = $this->data->purchase->id;
        $pi->item_id = $item_id;
        $pi->count = $count;
        if (!params("purchase_item_price")) continue;
        $pi->price = params("purchase_item_price")[$item_id];
        $result = $pi->save();
        $success = $success and $result;
      }
    }

    if ($success) {
      $this->render("purchases/create_success");
    } else {
      $this->data->errors = $this->data->purchase->errors->fetch();
      $this->render("purchases/create_errors");
    }
  }

  public function destroy($id) {
    restrict("destroy", "purchases");
    try {
      $purchase = Purchase::find($id);
    } catch (Exception $e) {
      $this->render("purchases/destroy_success");
    }
    if ($purchase->delete()) {
      $this->render("purchases/destroy_success");
    } else {
      $this->render("purchases/destroy_errors");
    }
  }
}