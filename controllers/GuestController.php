<?php

class GuestController extends Controller {
  function __construct() {
    parent::__construct();
  }

  public function index() {
    $this->data->pagination = Item::paginate();
    $this->render("guest/index");
  }

  public function login() {
    $this->render("guest/login");
  }

  public function logout() {
    User::logout();
    header("Location:".u("guest#index"));
  }

  public function auth() {
    $authInfo = params("user");
    $username = $authInfo["username"];
    $password = $authInfo["password"];

    if (User::login($username, $password)) {
      $this->render("guest/auth_success");
    } else {
      $this->render("guest/auth_failed");
    }
  }

  public function payment_confirmation() {
    $this->render("guest/payment_confirmation");
  }

  public function save_payment_confirmation() {
    $sale = Sale::find_by_payment_confirmation_code(params("payment_confirmation_code"));
    if (!$sale) {
      $this->render("guest/payment_confirmation_failed");
    }

    if ($sale->confirmed) {
      $this->render("guest/sale_already_confirmed");
      return true;
    }

    if (!isset($_FILES["payment_confirmation_image"])) {
      $this->render("guest/payment_confirmation_failed");
    }

    $img = $_FILES["payment_confirmation_image"];
    $folder = "public/img/payment_confirmations/";
    $filename = $img["name"];

    if ($img["error"] == UPLOAD_ERR_OK) {
      $path = $folder. $filename;
      while (file_exists($path)) {
        $filename = str_replace(".", "_.", $filename);
        $path = $folder . $filename;
      }
      move_uploaded_file($img["tmp_name"], $path);

      $old_image = $sale->payment_confirmation_image;
      $sale->payment_confirmation_image = $filename;
      
      if ($sale->save()) {
        @unlink($folder. $old_image);
        $this->render("guest/payment_confirmation_success");
      } else {
        $this->render("guest/payment_confirmation_failed");
      }
    } else {
      $this->render("guest/payment_confirmation_failed");
    }
  }
}