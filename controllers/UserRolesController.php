<?php

class UserRolesController extends Controller {
  public function findUser($id) {
    try {
      $this->data->user = User::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
  }

  public function index($user_id) {
    restrict("read", "user_roles");
    $this->findUser($user_id);
    $this->data->user_roles = $this->data->user->user_roles;
    $this->render("users/roles/index");
  }

  public function add($user_id){
    restrict("create", "user_roles");
    $this->findUser($user_id);
    $this->data->roles = Role::all(array(
      "order" => "name",
      "conditions" => array("id NOT IN (SELECT role_id FROM user_roles WHERE user_id = ?)", $user_id)
    ));
  	$this->data->action = u("user_user_roles#create", array("user_id"=>$user_id)); 
    $this->data->method = "post"; 
    $this->render("users/roles/add");
  }

  public function create($user_id) {
    restrict("create", "user_roles");
    $this->findUser($user_id);
    $this->data->user_role = new UserRole();
    foreach (params("user_role") as $key => $value) {
      $this->data->user_role->$key = $value;
    }
    if ($this->data->user_role->save()) {
      $this->render("users/roles/create_success");
    } else {
      $this->data->errors = $this->data->user_role->errors->fetch();
      $this->render("users/roles/create_errors");
    }
  }

  public function destroy($id) {
    restrict("delete", "user_roles");
    try {
      $user_role = UserRole::find($id);
    } catch (Exception $e) {
      $this->render("users/roles/destroy_success");
    }
    if ($user_role->delete()) {
      $this->render("users/roles/destroy_success");
    } else {
      $this->render("users/roles/destroy_errors");
    }
  }
}