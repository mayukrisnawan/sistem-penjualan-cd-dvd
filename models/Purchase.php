<?php

class Purchase extends Model {
  static $belongs_to = array(
    array("supplier")
  );

  static $has_many = array(
  	array("purchase_items")
  );

  public function before_destroy() {
    foreach ($this->purchase_items as $purchase_item) {
      $purchase_item->delete();
    }
  }
}