<?php

class ApplicationFeature extends Model {
  static $has_many = array(
    array("role_application_features"),
    array("roles", "through" => "role_application_features")
  );

  public function is_manage_for($role_id) {
  	$raf = RoleApplicationFeature::first(array(
  		"conditions"=> array("role_id = ? AND application_feature_id=?", $role_id, $this->id)
  	));
  	if (!$raf) return false;
  	return $raf->c == 1 && $raf->r == 1 && $raf->u == 1 && $raf->d == 1;
  }

  public function is_create_for($role_id) {
  	$raf = RoleApplicationFeature::first(array(
  		"conditions"=> array("role_id = ? AND application_feature_id=?", $role_id, $this->id)
  	));
    if (!$raf) return false;
  	return $raf->c === 1;
  }

  public function is_read_for($role_id) {
  	$raf = RoleApplicationFeature::first(array(
  		"conditions"=> array("role_id = ? AND application_feature_id=?", $role_id, $this->id)
  	));
    if (!$raf) return false;
  	return $raf->r === 1;
  }

  public function is_update_for($role_id) {
  	$raf = RoleApplicationFeature::first(array(
  		"conditions"=> array("role_id = ? AND application_feature_id=?", $role_id, $this->id)
  	));
    if (!$raf) return false;
  	return $raf->u === 1;
  }

  public function is_delete_for($role_id) {
  	$raf = RoleApplicationFeature::first(array(
  		"conditions"=> array("role_id = ? AND application_feature_id=?", $role_id, $this->id)
  	));
    if (!$raf) return false;
  	return $raf->d === 1;
  }
}