<?php

class PurchaseItem extends Model {
  static $belongs_to = array(
    array("purchase"),
    array("item")
  );

  public function after_validation_on_create() {
  	$item = Item::find($this->item_id);
  	$item->count += $this->count;
  	$item->save();
  }

  public function after_validation_on_update() {
  	$pi = PurchaseItem::find($this->id);
  	$item = Item::find($this->item_id);
  	$item += $pi->count - $this->count;
  	if ($item->count < 0) $this->item->count = 0;
  	$item->save();
  }

 	public function before_destroy() {
 		$item = Item::find($this->item_id);
  	$item->count -= $this->count;
  	if ($item->count < 0) $item->count = 0;
  	$item->save();
  }
}