<?php

class UserRole extends Model {
  static $belongs_to = array(
    array("user"),
    array("role")
  );

  public function after_validation() {
    try {
      $role = Role::find($this->role_id);
    } catch (Exception $e) {
      $this->errors->add("role_id", "jabatan pengguna belum dipilih");
    }
  }

  public function after_validation_on_create() {
    $user_role = UserRole::first(array(
      "conditions" => array("role_id = ? AND user_id = ?", $this->role_id, $this->user_id)
    ));
    if ($user_role) $this->errors->add("role_id", "jabatan pengguna sudah digunakan");
  }

  public function after_validation_on_update() {
    $user_role = UserRole::first(array(
      "conditions" => array(
        "role_id = ? AND user_id = ? AND id <> ?", 
        $this->role_id, $this->user_id, $this->id
      )
    ));
    if ($user_role) $this->errors->add("role_id", "jabatan pengguna sudah digunakan");
  }
}