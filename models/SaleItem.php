<?php

class SaleItem extends Model {
  static $belongs_to = array(
    array("sale"),
    array("item")
  );
}