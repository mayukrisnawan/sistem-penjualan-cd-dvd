<?php

class Config extends Model {
	public static function get($key) {
		$config = Config::first(array(
			"conditions" => array("key"=>$key)
		));
		return $config ? $config->value : false;
	}

	public static function set($key, $value) {
		$config = Config::first(array(
			"conditions" => array("key"=>$key)
		));
		if (!$config) return false;
		$config->value = $value;
		return $config->save();
	}
}