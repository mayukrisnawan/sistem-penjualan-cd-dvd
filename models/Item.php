<?php

class Item extends Model {
  static $belongs_to = array(
    array("type")
  );

  static $has_many = array(
    array("item_tags"),
    array("tags", "through"=>"item_tags"),
    array("sale_items"),
    array("sales", "through"=>"sale_items")
  );

  static $validates_presence_of = array(
    array("title", "message"=>"nama barang tidak boleh kosong"),
    array("price", "message"=>"harga barang harus diisi"),
    array("type_id", "message"=>"tipe barang harus diisi")
  );
  static $validates_format_of = array(
    array("title", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );
  static $validates_numericality_of = array(
    array("price", "only_integer"=>true, "message"=>"harus berupa bilangan bulat"),
    array("price", "greater_than_or_equal_to"=>1, "message"=>"tidak boleh kurang dari satu")
  );

  // public function after_validation() {
  //   $item_count = Item::count(array(
  //     "conditions"=>array("title = ? AND type_id = ?", $this->title, $this->type_id)
  //   ));
  //   if ($item_count != 0) {
  //     $this->errors->add("title", "barang sudah tersedia pada daftar");
  //   }
  // }

  public function after_validation_on_create() {
    $this->count = 0;
    $this->created_at = date("Y-m-d H:i:s");
  }

  public function before_destroy() {
    ItemTag::delete_all(array(
      'conditions'=>array('item_id'=>$this->id)
    ));
    Discount::delete_all(array(
      'conditions'=>array('item_id'=>$this->id)
    ));
  }

  public static function search($query) {
    $items = Item::all(array(
      "conditions" => array("title LIKE ?", "%" . params("query") . "%"),
      "order" => "title"
    ));

    $typed_items = Item::all(array(
      "conditions" => array(
        "type_id IN (SELECT id FROM types WHERE name LIKE ?)", "%" . params("query") . "%"
      ),
      "order" => "title"
    ));

    foreach ($typed_items as $typed_item) {
      $found = false;
      foreach ($items as $item) {
        if ($typed_item->id == $item->id) {
          $found = true;
          break;
        }
      }
      if (!$found) {
        array_push($items, $typed_item);
      }
    }

    $tagged_items = Item::all(array(
      "conditions" => array("
        id IN
        (
          (SELECT item_id FROM item_tags WHERE tag_id IN 
            (SELECT id FROM tags WHERE title LIKE ?)
          )
        )", 
            "%" . params("query") . "%"),
      "order" => "title"
    ));

    foreach ($tagged_items as $tagged_item) {
      $found = false;
      foreach ($items as $item) {
        if ($tagged_item->id == $item->id) {
          $found = true;
          break;
        }
      }
      if (!$found) {
        array_push($items, $tagged_item);
      }
    }
    return $items;
  }

  public function discount_rate() {
    $discount = Discount::first(array(
      "conditions"=>array(
        "item_id = ? AND start_date <= DATE(NOW()) AND finish_date >= DATE(NOW())",
        $this->id
      )
    ));
    return $discount ? $discount->rate : 0;
  }
}