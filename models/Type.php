<?php

class Type extends Model {
  static $validates_presence_of = array(
    array("name", "message"=>"nama tipe barang tidak boleh kosong")
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );
  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"terlalu panjang, panjang maksimal adalah 100 karakter"),
    array("name", "minimum"=>2, "too_short"=>"minimal terdiri dari 2 karakter")
  );
  static $validates_uniqueness_of = array(
    array("name", "message"=>"nama tipe barang sudah digunakan, gunakan nama yang lain")
  );

  public function before_destroy() {
    Item::delete_all(array(
      'conditions'=>array('type_id'=>$this->id)
    ));
  }
}