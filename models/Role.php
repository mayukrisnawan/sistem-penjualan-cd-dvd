<?php

class Role extends Model {
  static $has_many = array(
    array("user_roles"),
    array("users", "through"=>"user_roles"),
    
    array("role_application_features"),
    array("application_features", "through" => "role_application_features")
  );
  
  static $validates_presence_of = array(
    array("name", "message"=>"nama jabatan pengguna tidak boleh kosong")
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );
  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"terlalu panjang, panjang maksimal adalah 100 karakter"),
    array("name", "minimum"=>2, "too_short"=>"minimal terdiri dari 2 karakter")
  );
  static $validates_uniqueness_of = array(
    array("name", "message"=>"nama jabatan pengguna sudah digunakan, gunakan nama yang lain")
  );

  public function setApplicationFeatures($application_features) {
    foreach ($application_features as $key => $value) {
      $raf = new RoleApplicationFeature();
      $af = ApplicationFeature::first(array(
        "conditions" => array("key"=>$key)
      ));

      if ($af) {
        $raf->application_feature_id = $af->id;
        $raf->role_id = $this->id;
        $raf->c = $value["c"] == 'true' ? 1 : 0;
        $raf->r = $value["r"] == 'true' ? 1 : 0;
        $raf->u = $value["u"] == 'true' ? 1 : 0;
        $raf->d = $value["d"] == 'true' ? 1 : 0;
        $raf->save();
      }
    }
  }
}