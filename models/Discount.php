<?php

class Discount extends Model {
  static $belongs_to = array(
    array("item")
  );

  static $validates_presence_of = array(
    array("start_date", "message"=>"tanggal mulai tidak boleh kosong"),
    array("finish_date", "message"=>"tanggal berakhir tidak boleh kosong"),
    array("rate", "message"=>"presentase diskon harus diisi"),
    array("item_id", "message"=>"barang harus dipilih")
  );
  // static $validates_numericality_of = array(
  //   array('rate', 'less_than_or_equal_to' => 1, 'greater_than_or_equal_to' => 0, "message" => "presentase diskon tidak valid")
  // );
}