<?php

class User extends Model {
  static $has_many = array(
    array("user_roles"),
    array("roles", "through"=>"user_roles")
  );

  static $validates_presence_of = array(
    array("username", "message"=>"tidak boleh kosong"),
    array("password", "message"=>"tidak boleh kosong")
  );
  static $validates_length_of = array(
    array("username", "minimum"=>5, "too_short"=>"minimal 5 karakter"),
    array("username", "maximum"=>100, "too_long"=>"tidak boleh lebih dari 100 karakter"),
    array("password", "maximum"=>100, "too_long"=>"tidak boleh lebih dari 100 karakter")
  );
  static $validates_uniqueness_of = array(
    array("username", "message"=>"username sudah digunakan, gunakan yang lain")
  );

  public static function logout() {
    unset($_SESSION['user_id']);
  }

  public static function login($username, $password) {
    $user = User::first(array(
      "conditions" => array("username = ? AND password = ?", $username, md5($password))
    ));

    if ($user) {
      $_SESSION['user_id'] = $user->id;
      return true;
    } else {
      return false;
    }
  }
}