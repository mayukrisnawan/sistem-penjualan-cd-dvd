<?php

class Sale extends Model {
  static $has_many = array(
    array("sale_items"),
    array("items", "through"=>"sale_items")
  );

  static $validates_presence_of = array(
    array("email", "message"=>"email tidak boleh kosong"),
    array("address", "message"=>"alamat tidak boleh kosong")
  );

  public function before_destroy() {
    SaleItem::delete_all(array(
      "conditions" => array("sale_id" => $this->id)
    ));
  }

  public function after_validation_on_create() {
    $this->confirmed = 0;
    $this->generate_payment_confirmation_code();
  }

  public function generate_payment_confirmation_code() {
    $charset = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    $code = "";
    for ($i=0; $i<10; $i++) {
      $code .= array_rand($charset);
    }

    $sale = Sale::first(array(
      "conditions" => array("payment_confirmation_code" => $code)
    ));

    if ($sale) {
      $this->generate_payment_confirmation_code();
    } else {
      $this->payment_confirmation_code = $code;
    }
  }

  public function send_confirmation() {
    $body = Config::get("sale_payment_reply");

    $body .= "\r\nDetail Transaksi :\r\n";
    foreach ($this->sale_items as $index=>$sale_item) {
      try {
        $item = Item::find($sale_item->item_id);
      } catch (Exception $e) {
        continue;
      }
      $body .= ($index+1) . 
           ". " . $sale_item->item->type->name . 
           " " . $sale_item->item->title . 
           " | " . $sale_item->count . 
           "pcs @ Rp" . $sale_item->price . 
           " (diskon ". ($sale_item->discount_rate * 100) ."%)" .
           " | Rp " . ($sale_item->price * (1-$sale_item->discount_rate)) . "\r\n";
    }

    $body .= "\r\n";   
    $body .= "Jumlah Pembayaran : Rp " . $this->total_price();
    $body .= "\r\n";   
    $body .= "Kode Konfirmasi Pembayaran : " . $this->payment_confirmation_code;   

    $from = "sistempenjualancddvd@gmail.com";
    $to = $this->email;
    $subject = "Konfirmasi Pembayaran";

    $headers = array(
	    'From' => $from,
	    'To' => $to,
	    'Subject' => $subject
		);

		$smtp = Mail::factory('smtp', array(
      'host' => 'ssl://smtp.gmail.com',
      'port' => '465',
      'auth' => true,
      'username' => 'sistempenjualancddvd@gmail.com',
      'password' => 'sistempenjualancddvd123'
	  ));

    // echo $body;
		$mail = $smtp->send($to, $headers, $body);
		return !PEAR::isError($mail);
    // return true;
  }

  public function total_price() {
    $total = 0;
    foreach ($this->sale_items as $sale_item) {
      try {
        $item = Item::find($sale_item->item_id);
      } catch (Exception $e) {
        continue;
      }
      $total += $sale_item->price * $sale_item->count * (1 - $sale_item->discount_rate);
    }
  	return $total;
  }

  public function reduce_items_count() {
    foreach ($this->sale_items as $sale_item) {
      try {
        $item = Item::find($sale_item->item_id);
      } catch (Exception $e) {
        continue;
      }
      $item->count -= $sale_item->count;
      if ($item->count < 0) $item->count = 0;
      $item->save(); 
    }
  }

  public function payment_conformation_image_path() {
    return "payment_confirmations/" . $this->payment_confirmation_image;
  }
}