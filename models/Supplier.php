<?php

class Supplier extends Model {
  static $validates_presence_of = array(
    array("name", "message"=>"nama supplier tidak boleh kosong")
  );
  static $validates_length_of = array(
    array("name", "maximum"=>200, "too_long"=>"terlalu panjang, panjang maksimal adalah 200 karakter"),
    array("name", "minimum"=>2, "too_short"=>"minimal terdiri dari 2 karakter")
  );
  static $validates_uniqueness_of = array(
    array("name", "message"=>"nama supplier sudah digunakan, gunakan nama yang lain")
  );
}