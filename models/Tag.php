<?php

class Tag extends Model {
  static $has_many = array(
  	array("item_tags"),
    array("items", "through"=>"item_tags")
  );

  public function before_destroy() {
    ItemTag::delete_all(array(
      'conditions'=>array('tag_id'=>$this->id)
    ));
  }
}