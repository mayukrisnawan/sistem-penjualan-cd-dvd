<?php

class RoleApplicationFeature extends Model {
  static $belongs_to = array(
    array("application_feature"),
    array("role")
  );
}