<?php

class ItemTag extends Model {
  static $belongs_to = array(
  	array("item"),
  	array("tag")
  );

  public static function create($params) {
  	$item_tag = new ItemTag();
  	$item_tag->item_id = $params["item_id"];
  	$params["name"] = strtolower($params["name"]);
  	$tag = Tag::first(array(
  		"conditions" => array("title" => $params["name"])
  	));
  	if (!$tag) {
  		$tag = new Tag();
  		$tag->title = $params["name"];
  		$tag->save();
  	}
  	$item_tag->tag_id = $tag->id;
  	$old_item_tag = ItemTag::first(array(
			"conditions" => array("item_id = ? AND tag_id = ?", $params["item_id"], $tag->id)
		));

		if ($old_item_tag) {
			return $old_item_tag;
		}
  	return $item_tag;
  }
}