<?php

function appAutoload($name) {
  $paths = array(
    "controllers/".$name."Controller.php",
    "controllers/".$name.".php"
  );
  foreach ($paths as $path) {
    if (file_exists($path)) {
      require_once($path);
    }
  }
}
spl_autoload_register("appAutoload");
require_once "system/App.php";

try {
  App::run(); 
} catch (Exception $e) {
  echo "<h1>".$e->getMessage()."</h1>";
  echo "<hr/>";
  echo "<h2>Urutan lokasi error :</h2>";
  echo "<ol>";
  foreach ($e->getTrace() as $trace) {
    $source = isset($trace["file"]) ? $trace["file"] : "";
    $source .= isset($trace["class"]) ? " <i>" . $trace["class"] . "</i> ->" : "";
    $source .= isset($trace["function"]) ? " <i>" . $trace["function"] . "</i>" : "";
    $source .= isset($trace["line"]) ? " <b>[Baris ke : " . $trace["line"] ."]</b>" : "";
    echo  "<li>".$source."</li>";
  }
  echo "</ol>";
}