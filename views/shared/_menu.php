<div class="navbar-collapse collapse">
	<ul class="nav navbar-nav">
	  <li class="vertical-divider"></li>
	  {% if not current_user() %}
	  <li>
	  	<a href="{{ u('guest#payment_confirmation') }}">Konfirmasi Pembayaran</a>
	  </li>
	  {% endif %}
	  <!-- Penjualan -->
	  {% if can("read", "discounts") or can("read", "sales") %}
		  <li class="dropdown">
		    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		      Penjualan <span class="caret"></span>
		    </a>
		    <ul class="dropdown-menu">
		    	{% if can("read", "sales") %}
		      	<li><a href="{{ u('sales#index') }}">Daftar Penjualan</a></li>
			      <!-- <li><a href="">Laporan Penjualan</a></li> -->
		      {% endif %}
		      {% if can("manage", "sales") %}
		      	<li><a href="{{ u('sale_payment_reply') }}">Edit Pesan Konfirmasi Pembayaran</a></li>
		      {% endif %}
		      {% if can("read", "discounts") %}
		      	<li><a href="{{ u('discounts#index') }}">Diskon</a></li>
		      {% endif %}
		    </ul>
		  </li>
		 {% endif %}
	  <!-- /Penjualan -->

	  <!-- Pembelian -->
	  {% if can("read", "purchases") or can("read", "suppliers") %}
	  <li class="dropdown">
	    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	      Pembelian <span class="caret"></span>
	    </a>
	    <ul class="dropdown-menu">
	    	{% if can("read", "purchases") %}
		      <li><a href="{{ u('purchases#index') }}">Daftar Pembelian</a></li>
		      <!-- <li><a href="">Laporan Pembelian</a></li> -->
		    {% endif %}
		    {% if can("read", "suppliers") %}
	      	<li><a href="{{ u('suppliers#index') }}">Daftar Supplier</a></li>
	    	{% endif %}
	    </ul>
	  </li>
	  {% endif %}
	  <!-- /Pembelian -->

	  <!-- Barang -->
	  {% if can("read", "items") or can("read", "types") or can("read", "tags") %}
		  <li class="dropdown">
		    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		      Barang <span class="caret"></span>
		    </a>
		    <ul class="dropdown-menu">
		      <li><a href="{{ u('items#index') }}">Daftar Barang</a></li>
		      <li><a href="{{ u('types#index') }}">Tipe Barang</a></li>
		      <li><a href="{{ u('tags#index') }}">Kata Kunci Barang</a></li>
		    </ul>
		  </li>
		 {% endif %}
	  <!-- /Barang -->

	  <!-- Registrasi Pengguna -->
	  {% if can("read", "users") or can("read", "roles") %}
		  <li class="dropdown">
		    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		      Registrasi Pengguna <span class="caret"></span>
		    </a>
		    <ul class="dropdown-menu">
		    	{% if can("read", "users") %}
		      	<li><a href="{{ u('users#index') }}">Daftar Pengguna</a></li>
		      {% endif %}
		      {% if can("read", "roles") %}
		      	<li><a href="{{ u('roles#index') }}">Daftar Jabatan Pengguna</a></li>
		      {% endif %}
		    </ul>
		  </li>
	  {% endif %}
	  <!-- /Registrasi Pengguna -->
	</ul>
	<ul class="nav navbar-nav navbar-right">
	  {% if current_user() %}
		  <li class="dropdown">
		    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		      {{ current_user().username }} <span class="caret"></span>
		    </a>
		    <ul class="dropdown-menu">
		      <li><a href="{{ u('change_password_page') }}">Ganti Password</a></li>
		      <li><a href="{{ u('guest#logout') }}">Logout</a></li>
		    </ul>
		  </li>
		  <li>
		  	<a href="{{ u('guest#payment_confirmation') }}" title='konfirmasi pembayaran'>
		  		<i class='glyphicon glyphicon-check'></i>
		  	</a>
		  </li>	
	  {% else %}
		  <li>
	  		<a href="{{ u('guest#login') }}">Login</a>
		  </li>
  	{% endif %}
	  <li>
	  	<a href="{{ u('shopping_cart#show') }}" title='keranjang belanja'>
	  		<i class='glyphicon glyphicon-shopping-cart'></i>
	  		<label class='badge'><?php echo count(ShoppingCart::all()); ?></label>
	  	</a>
	  </li>
	</ul>
</div>