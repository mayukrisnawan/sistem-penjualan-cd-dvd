{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Jabatan Pengguna</h1>
<hr/>
{% if can('create', 'roles') %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah jabatan pengguna baru", "roles#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Jabatan</th>
        {% if can('delete', 'roles') or can('update', 'roles') or can('update', 'role_application_features') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('delete', 'roles') or can('update', 'roles') or can('update', 'role_application_features') ? 3 : 2 }}">
            <center>
              <i>Belum ada jabatan yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for role in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ role.name }}
          </td>

          {% if can('delete', 'roles') or can('update', 'roles') or can('update', 'role_application_features') %}
            <td>
              {% if can('update', 'roles') %}
                {{ 
                  a("Edit", "roles#edit", {id:role.id}) 
                }}
              {% endif %}
              {% if can('delete', 'roles') and can('update', 'roles') %}
              |
              {% endif %}
              {% if can('delete', 'roles') %}
                {{
                  a("Hapus", "roles#destroy", {id:role.id}, {method:"delete", remote:"true", 
                    confirmTitle:"Jabatan Pengguna",
                    confirmMessage:"Apakah anda yakin ingin menghapus jabatan pengguna berikut?",
                    class:"btn_hapus"
                  }) 
                }}
              {% endif %}
              {% if can('delete', 'roles') and can('update', 'role_application_features') %}
              |
              {% endif %}
              {% if can('update', 'role_application_features') %}
              {{
                a("Edit Hak Akses Pengguna", "role_role_application_features#index", { role_id: role.id })
              }}
              {% endif %}
            </td>
          {% endif %}

        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}