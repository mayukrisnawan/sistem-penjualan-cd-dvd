$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> jabatan pengguna berhasil ditambahkan, {{ a('Kembali','roles#index') }}")
           .show();
$("form[name='role_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='role_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");