{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Hak Akses Jabatan Pengguna >> {{ role.name }}</h1>
<a href="" class='btn btn-info' id="btn_simpan_hak_akses">
  <i class='glyphicon glyphicon-ok'></i>
  &nbsp;
  Simpan Hak Akses
</a>
<hr/>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%"></th>
        <th>Hak Akses</th>
        <th width="10%">Tambah</th>
        <th width="10%">Baca</th>
        <th width="10%">Perbaharui</th>
        <th width="10%">Hapus</th>
      </tr>
    </thead>
    <tbody>
      {% for application_feature in application_features %}
        <tr class='application_feature_row' data-key='{{ application_feature.key }}'>
        	<td>
        		<input type='checkbox' class='check_all' data-key='{{ application_feature.key }}' {{ application_feature.is_manage_for(role.id) ? 'checked' : '' }}/>
        	</td>
          <td>{{ application_feature.description }}</td>
          <td>
            <input type='checkbox' class='check-create' data-key='{{ application_feature.key }}' {{ application_feature.is_create_for(role.id) ? 'checked' : '' }}/>
          </td>
        	<td>
        		<input type='checkbox' class='check-read' data-key='{{ application_feature.key }}' {{ application_feature.is_read_for(role.id) ? 'checked' : '' }}/>
        	</td>
        	<td>
        		<input type='checkbox' class='check-update' data-key='{{ application_feature.key }}' {{ application_feature.is_update_for(role.id) ? 'checked' : '' }}/>
        	</td>
        	<td>
        		<input type='checkbox' class='check-delete' data-key='{{ application_feature.key }}' {{ application_feature.is_delete_for(role.id) ? 'checked' : '' }}/>
        	</td>
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
<script type="text/javascript">
	$("body").on('click', '.check_all', function(){
		var key = $(this).attr("data-key");
		var checked = this.checked;
		$(".check-read[data-key='" + key  + "']").get(0).checked = checked;
		$(".check-create[data-key='" + key  + "']").get(0).checked = checked;
		$(".check-update[data-key='" + key  + "']").get(0).checked = checked;
		$(".check-delete[data-key='" + key  + "']").get(0).checked = checked;
	});

  function collect_application_features() {
    var result = {};
    $(".application_feature_row").each(function(){
      var afr_key = $(this).attr("data-key");
      if (!result[afr_key]) result[afr_key] = {};
      result[afr_key].c = $(this).find(".check-create").get(0).checked;
      result[afr_key].r = $(this).find(".check-read").get(0).checked;
      result[afr_key].u = $(this).find(".check-update").get(0).checked;
      result[afr_key].d = $(this).find(".check-delete").get(0).checked;
    });
    return result;
  }

  $("#btn_simpan_hak_akses").click(function(){
    var $btn = $(this).attr("disabled", "disabled");
    var application_features = collect_application_features();

    $.ajax({
      url:"{{ u('update_application_features_role', { id: role.id }) }}",
      data: {
        application_features: application_features
      },
      method:'patch',
      dataType:'script',
      complete:function(){
        $btn.removeAttr("disabled");
      }
    });
    return false;
  });
</script>
{% endblock %}