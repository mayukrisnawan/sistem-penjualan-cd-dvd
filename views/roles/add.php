{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Jabatan Pengguna Baru</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'roles/form.php' %}
  </div>
</div>
{% endblock %}