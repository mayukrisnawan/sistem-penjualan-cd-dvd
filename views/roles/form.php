<form role="form" name='role_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="role[id]" value="{{ role.id }}">
  <div class="form-group">
    <label class="control-label">Nama Jabatan</label>
    <input name="role[name]" type="text" class="form-control" value="{{ role.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("role"); ?>
</script>