<div class="table-responsive" id="items_container" style="max-height:400px; overflow-y:scroll">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Barang</th>
        <th>Harga Barang</th>
        <th>Tipe Barang</th>
        <th>Jumlah Barang</th>
        <th>Pilihan</th>
      </tr>
    </thead>
    <tbody>
      {% if items|length == 0 %}
        <tr>
          <td colspan="6">
            <center>
              <i>Data Kosong.</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for item in items %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ item.title }}
          </td>
          <td>
            {{ item.price }}
          </td>
          <td>
            {{ item.type.name }}
          </td>
          <td>
            {{ item.count }}
          </td>
          <td>
            <a href="" class='btn-select-item' data-id="{{ item.id }}" data-title="{{ item.title }}" data-type="{{ item.type.name }}">&lt;- Pilih Barang</a>
          </td>
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>