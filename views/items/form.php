<form role="form" name='item_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="item[id]" value="{{ item.id }}">

  <div class="form-group">
    <label class="control-label">Nama Barang</label>
    <input name="item[title]" type="text" class="form-control" value="{{ item.title }}"/>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Harga Barang</label>
    <input name="item[price]" type="number" class="form-control" value="{{ item.price ? item.price : 0 }}"/>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Tipe Barang</label>
    <select name="item[type_id]" class="form-control">
    {% for type in types %}
      <option value="{{ type.id }}" {{ item.type_id == type.id ? "selected" : "" }}>{{ type.name }}</option>
    {% endfor %}
    </select>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("item"); ?>
</script>