{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Barang Baru</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'items/form.php' %}
  </div>
</div>
{% endblock %}