<h3>Pencarian Barang</h3>
<hr>
<div class='input-group'>
  <input type='text' class='form-control' id="item_search_input"/>
  <span class='input-group-addon'>
    <i class='glyphicon glyphicon-search'></i>
  </span>
</div>
<hr>
<div id="item_search_result_container"></div>

<script type="text/javascript">
(function(){
	var xhr_search = null;

	var searchItem = function(query){
		if (xhr_search) xhr_search.abort();
		$("#item_search_input").attr("disabled", "disabled");
		xhr_search = $.ajax({
			url:"{{ u('items#search') }}",
			data: "query=" + query,
			success: function(response){
				$("#item_search_result_container").html(response);
				if (search_item_callback) search_item_callback();
			},
			complete: function(){
				$("#item_search_input").removeAttr("disabled");
			}
		});
	}

	$("#item_search_input").keyup(function(e){
		if (e.keyCode == 13) {
			var query = $("#item_search_input").val();
			searchItem(query);
		}
		return false;
	});

})();
</script>