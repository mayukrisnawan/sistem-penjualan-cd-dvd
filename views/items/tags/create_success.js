$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> kata kunci berhasil ditambahkan, {{ a('Kembali','item_item_tags#index', { item_id:item.id }) }}")
           .show();
$("form[name='item_tag_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='item_tag_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");