{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Kata Kunci >> {{ item.title }}</h1>
<hr/>
{% if can('create', 'item_tags') %}
  <form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
      {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah kata kunci", "item_item_tags#add", { item_id : item.id }, {class:"btn btn-success form-control"}) }}
    </div>
  </form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Kata Kunci</th>
        {% if can('delete', 'item_tags') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if item_tags|length == 0 %}
        <tr>
          <td colspan="{{ can('delete', 'item_tags') ? 3 : 2 }}">
            <center>
              <i>Belum ada kata kunci yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for item_tag in item_tags %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ item_tag.tag.title }}
          </td>
          {% if can('delete', 'item_tags') %}
          <td>
            {{
              a("Hapus", "item_tags#destroy", {id:item_tag.id}, {method:"delete", remote:"true", 
                confirmTitle:"Kata Kunci",
                confirmMessage:"Apakah anda yakin ingin menghapus kata kunci berikut?",
                class:"btn_hapus"
              }) 
            }}
          </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{% endblock %}