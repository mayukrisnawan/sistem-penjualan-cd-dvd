{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Kata Kunci Baru >> {{ item.title }}</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'items/tags/form.php' %}
  </div>
</div>
{% endblock %}