<form role="form" name='item_tag_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="item_tag[id]" value="{{ item_tag.id }}">
  <input type="hidden" name="item_tag[item_id]" value="{{ item.id }}">
  <div class="form-group">
    <label class="control-label">Kata Kunci</label>
    <input name="item_tag[name]" type="text" class="form-control" value="{{ item_tag.name }}" id="search_tag"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("item_tag"); ?>
</script>

<script type="text/javascript">
  $("#search_tag").typeahead({
    source: function (query, typeahead) {
      return $.getJSON("{{ u('tags#json') }}", { content: query }, function (data) {
        var tags = [];
        for (var index in data) {
          tags.push(data[index].title);
        }
        return typeahead(tags);
      });
    }
  });
</script>