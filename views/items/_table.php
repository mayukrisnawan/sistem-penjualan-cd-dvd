<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Barang</th>
        <th>Harga Barang</th>
        <th>Tipe Barang</th>
        <th>Jumlah Barang</th>
        {% if can('update', 'items') or can('delete', 'items') or can('read', 'item_tags') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('update', 'items') or can('delete', 'items') or can('read', 'item_tags') ? 6 : 5 }}">
            <center>
              <i>Belum ada barang yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for item in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ item.title }}
          </td>
          <td>
            {{ item.price }}
          </td>
          <td>
            {{ item.type.name }}
          </td>
          <td>
            {{ item.count }}
          </td>

          {% if not for_search %}
            {% if can('update', 'items') or can('delete', 'items') or can('read', 'item_tags') %} 
            <td>
              {% if can('update', 'items') %}
                {{ 
                  a("Edit Detail Barang", "items#edit", {id:item.id}) 
                }}
              {% endif %}
              {% if can('update', 'items') and can('delete', 'items') %}
              |
              {% endif %}

              {% if can('delete', 'items') %}
                {{
                  a("Hapus", "items#destroy", {id:item.id}, {method:"delete", remote:"true", 
                    confirmTitle:"Barang",
                    confirmMessage:"Apakah anda yakin ingin menghapus barang berikut?",
                    class:"btn_hapus"
                  }) 
                }}
              {% endif %}

              {% if can('update', 'items') and can('read', 'item_tags') %}
              |
              {% endif %}

              {% if can('read', 'item_tags') %}
                {{ 
                  a("Edit Kata Kunci", "item_item_tags#index", {item_id:item.id}) 
                }}
              {% endif %}
            </td>
            {% endif %}
          {% else %}
            <td>
              <a href="" class='btn-select-item' data-id="{{ item.id }}">&lt;- Pilih Barang</a>
            </td>
          {% endif %}

        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}