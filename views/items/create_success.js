$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> barang berhasil ditambahkan, {{ a('Kembali','items#index') }}")
           .show();
$("form[name='item_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='item_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");