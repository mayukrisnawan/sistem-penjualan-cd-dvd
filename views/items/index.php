{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Barang</h1>
<hr/>
{% if can('create', 'items') %}
	<form class="form-inline" style="margin : 10px auto">
	  <div class="form-group">
	    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah barang baru", "items#add", {}, {class:"btn btn-success form-control"}) }}
	  </div>
	</form>
{% endif %}
{% include "items/_table.php" %}
{% endblock %}