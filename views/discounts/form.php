<form role="form" name='discount_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="discount[id]" value="{{ discount.id }}">

  <div class="form-group">
    <label class="control-label">Nama Barang</label>
    <div class="input-group">
      <input name="discount[item_id]" type="hidden" id="item_id" value="{{ discount.item_id }}"/>
      <input readonly type="text" class="form-control" value="{{ discount.item.type.name}} {{discount.item.title }}" id="nama_barang"/>
      <span class="input-group-addon" id="select_item">
        <i class="glyphicon glyphicon-th"></i>
      </span>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Tanggal Mulai</label>
    <div class="input-group">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-calendar"></i>
      </span>
      <input name="discount[start_date]" type="text" class="form-control datepicker" date-format="dd-mm-yyyy" value="{{ discount.start_date.format('d-m-Y') }}"/>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Tanggal Berakhir</label>
    <div class="input-group">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-calendar"></i>
      </span>
      <input name="discount[finish_date]" type="text" class="form-control datepicker" date-format="dd-mm-yyyy" value="{{ discount.finish_date.format('d-m-Y') }}"/>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group" style="width:150px">
    <label class="control-label">Presentase Diskon</label>
    <div class="input-group">
      <input name="discount[rate]" type="text" class="form-control" value="{{ discount.rate ? discount.rate * 100 : 0 }}"/>
      <span class="input-group-addon">%</span>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("discount"); ?>
</script>

<script type="text/javascript">
  var search_item_callback = function(){
    $(".btn-select-item").click(function(){
      var item_id = $(this).attr("data-id");
      var title = $(this).attr("data-title");
      var type = $(this).attr("data-type");
      $("#item_id").val(item_id);
      $("#nama_barang").val(type + " " + title);
      return false;
    });
  }
</script>