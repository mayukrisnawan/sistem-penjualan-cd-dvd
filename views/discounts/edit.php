{% extends "layouts/application_layout.php" %}

{% block body %}
<?php
	js("items");
?>
<h1>Edit Diskon Penjualan</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'discounts/form.php' %}
  </div>
  <div class="col-md-6" id="item_search_container">
  	{% include "items/_search_items.php" %}
  </div>
</div>
{% endblock %}