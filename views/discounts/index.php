{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Diskon Penjualan</h1>
<hr/>
{% if can('create', 'discounts') %}
  <form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
      {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah aturan diskon baru", "discounts#add", {}, {class:"btn btn-success form-control"}) }}
    </div>
  </form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Barang</th>
        <th>Tipe Barang</th>
        <th>Tanggal Mulai</th>
        <th>Tanggal Berakhir</th>
        <th>Presentase</th>
        {% if can('update', 'discounts') and can('delete', 'discounts') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('update', 'discounts') and can('delete', 'discounts') ? 7 : 6 }}">
            <center>
              <i>Belum ada diskon yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for discount in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ discount.item.title }}
          </td>
          <td>
            {{ discount.item.type.name }}
          </td>
          <td>
            {{ discount.start_date.format('d/M/Y') }}
          </td>
          <td>
            {{ discount.finish_date.format('d/M/Y') }}
          </td>
          <td>
            {{ discount.rate * 100 }}%
          </td>

          {% if can('update', 'discounts') or can('delete', 'discounts') %}
            <td>
              {% if can('update', 'discounts') %}
                {{ 
                  a("Edit", "discounts#edit", {id:discount.id}) 
                }}
              {% endif %}
              {% if can('update', 'discounts') and can('delete', 'discounts') %}
              |
              {% endif %}
              {% if can('delete', 'discounts') %}
                {{
                  a("Hapus", "discounts#destroy", {id:discount.id}, {method:"delete", remote:"true", 
                    confirmTitle:"Diskon Penjualan",
                    confirmMessage:"Apakah anda yakin ingin menghapus diskon berikut?",
                    class:"btn_hapus"
                  }) 
                }}
              {% endif %}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}