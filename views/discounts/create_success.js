$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> diskon berhasil ditambahkan, {{ a('Kembali','discounts#index') }}")
           .show();
$("form[name='discount_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='discount_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");