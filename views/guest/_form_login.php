<form role="form" name="user_form" class="form-horizontal" action='{{ u("guest#auth") }}' method="POST">
  <div class="flash alert" style="display:none"></div>
  <div class="form-group">
    <label class="control-label">Username</label>
    <div>
      <input type="text" name="user[username]" class="form-control" placeholder="Username">
      <span class="help-block"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label">Password</label>
    <div>
      <input type="password" name="user[password]" class="form-control" placeholder="Password">
      <span class="help-block"></span>
    </div>
  </div>
  <div class="form-group">
    <div>
      <input type="submit" class="btn btn-default" value="Login"/>
    </div>
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("user"); ?>
</script>