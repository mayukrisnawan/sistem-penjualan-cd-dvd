<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Barang</th>
        <th>Harga Barang</th>
        <th>Diskon</th>
        <th>Tipe Barang</th>
        <th>Ketersedian</th>
        <th>Pilihan</th>
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="6">
            <center>
              <i>Belum ada barang yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for item in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ item.title }}
          </td>
          <td>
            Rp {{ item.price }}
          </td>
          <td>
            {% if item.discount_rate() %}
              <span class='label label-danger'>{{ item.discount_rate() * 100 }} %</span>
            {% else %}
              <span class='label label-warning'>Tidak Tesedia</span>
            {% endif %}
          </td>
          <td>
            {{ item.type.name }}
          </td>
          <td>
            {% if item.count == 0 %}
              <span class='label label-danger'>Stok Habis</span>
            {% else %}
              <span class='label label-success'>Stok Tersedia ({{ item.count }} pcs)</span>
            {% endif %}
          </td>

          <?php
            $item_count = ShoppingCart::itemCount("{{ item.id }}");
          ?>

          <td>
            <a class="btn btn-danger btn-buy-item" data-item-id='{{ item.id }}' <?php if ($item_count != 0 || {{ item.count }} == 0) echo "disabled"; ?> >
              <i class='glyphicon glyphicon-shopping-cart'></i>&nbsp;
              Beli ::
            </a>
          </td>
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
<script type="text/javascript">
  $(".btn-buy-item").click(function(){
    var item_id = $(this).attr("data-item-id");
    var count = prompt("Masukan jumlah barang!");
    count = (new Number(count)).valueOf();
    if (isNaN(count)) return false;

    $(".btn-buy-item").attr("disabled", "disabled");

    $.ajax({
      url:"{{ u('shopping_cart#add') }}",
      data:{
        item_id: item_id,
        count: count
      },
      dataType: "script",
      complete: function(){
        $(".btn-buy-item").removeAttr("disabled");
      }
    });
    return false;
  });
</script>