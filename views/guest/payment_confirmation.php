{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Konfirmasi Pembayaran</h1>
<div class='row'>
	<div class='col-md-6'>
		<form role="form" enctype="multipart/form-data" id="payment_confirmation_form" method="post" action="{{ u('guest#save_payment_confirmation') }}">
			<div class="form-group">
				<label class='control-label'>Kode Pembayaran</label>
				<input type='text' name="payment_confirmation_code" class='form-control'/>
			</div>
			<div class="form-group">
				<label class='control-label'>Bukti Transfer</label>
				<input type='file' name="payment_confirmation_image"/>
			</div>
			<div class="form-group">
				<input type='submit' class='btn btn-info' value="Kirim"/>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$("#payment_confirmation_form").ajaxForm({
	dataType:"script"
});
</script>
{% endblock %}