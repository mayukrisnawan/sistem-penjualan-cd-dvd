{% extends "layouts/application_layout.php" %}

{% block body %}
<div class="row" style="margin: 50px auto;">
  <div class="col-md-12">
    <h1><b>Halaman Utama</b></h1>
    <blockquote><i>"Selamat datang di Sistem Informasi Penjualan CD/DVD"</i></blockquote>
    <hr>
    {% include "guest/_item_table.php" %}
  </div>
</div>
{% endblock %}