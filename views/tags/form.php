<form role="form" name='tag_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="tag[id]" value="{{ tag.id }}">
  <div class="form-group">
    <label class="control-label">Kata Kunci</label>
    <input name="tag[title]" type="text" class="form-control" value="{{ tag.title }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("tag"); ?>
</script>