{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Kata Kunci Barang</h1>
<hr/>
{% if can('create', 'tags') %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah kata kunci baru", "tags#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Kata Kunci</th>
        {% if can('update', 'tags') or can('delete', 'tags') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('update', 'tags') or can('delete', 'tags') ? 3 : 2 }}">
            <center>
              <i>Belum ada kata kunci yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for tag in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ tag.title }}
          </td>
          {% if can('update', 'tags') or can('delete', 'tags') %}
            <td>
              {% if can('update', 'tags') %}
                {{ 
                  a("Edit", "tags#edit", {id:tag.id}) 
                }}
              {% endif %}
              {% if can('update', 'tags') and can('delete', 'tags') %}
              |
              {% endif %}
              {% if can('delete', 'tags') %}
              {{
                a("Hapus", "tags#destroy", {id:tag.id}, {method:"delete", remote:"true", 
                  confirmTitle:"Kata Kunci",
                  confirmMessage:"Apakah anda yakin ingin menghapus kata kunci berikut?",
                  class:"btn_hapus"
                }) 
              }}
              {% endif %}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}