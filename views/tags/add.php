{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Kata Kunci Barang Baru</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'tags/form.php' %}
  </div>
</div>
{% endblock %}