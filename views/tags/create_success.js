$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> kata kunci berhasil ditambahkan, {{ a('Kembali','tags#index') }}")
           .show();
$("form[name='tag_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='tag_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");