<form role="form" name='payment_form'>
  <div class="flash alert" style="display:none"></div>
  <div class="form-group">
    <label class="control-label">Judul</label>
    <textarea id="message_container" class="form-control" rows="7">{{ message }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <a class="btn btn-success" id="btn_submit">Simpan</a>
  </div>
</form>
<script type="text/javascript">
var $editor = $("#message_container");
// $editor.summernote({
//   height:300
// });

$("#btn_submit").click(function(){
  var btn = this;
  var content = $editor.val();
  var timer = loadDotsAnimationInto(btn, "Menyimpan");
  $(btn).attr("disabled", "disabled");
  $.ajax({
    url:'{{ u("update_payment_reply") }}',
    method:'patch',
    dataType:"script",
    data:"message="+encodeURIComponent(content),
    complete:function(){
      $(btn).removeAttr("disabled");
      stopDotsAnimationOn(timer, btn, "Simpan");
    }
  });
  return false;
});
</script>