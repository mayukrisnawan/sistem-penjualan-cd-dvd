{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Bukti Pembayaran</h1>
<hr/>
{% if sale.payment_confirmation_image %}
	{{ img(sale.payment_conformation_image_path()) }}
{% else %}
	<h4><i>Belum dikirim.</i></h4>
{% endif %}
{% endblock %}