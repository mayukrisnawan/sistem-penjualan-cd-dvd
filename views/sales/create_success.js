$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> keranjang belanja berhasil disimpan<br/>\
           	Konfirmasi pembayaran akan dikirim ke {{ sale.email }}, {{ a('Kembali','guest#index') }}")
           .show();
$("form[name='sale_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='sale_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");