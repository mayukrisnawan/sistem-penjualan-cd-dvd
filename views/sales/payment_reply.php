{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Edit</h1>
<div class="row">
  <div class="col-md-7">
    {% include 'sales/payment_reply_form.php' %}
  </div>
</div>
{% endblock %}