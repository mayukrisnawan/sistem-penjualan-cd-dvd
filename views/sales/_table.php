<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>Pesan</th>
        <th>Tanggal/Waktu</th>
        <th>Status</th>
        {% if can("update", "sales") %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('update', 'sales') ? 7 : 6 }}">
            <center>
              <i>Belum ada penjualan yang tercatat</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for sale in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>{{ sale.email }}</td>
          <td>{{ sale.address }}</td>
          <td>{{ sale.message }}</td>
          <td>{{ sale.created_at.format('d M Y/H:i:s') }}</td>
          <td>
            {% if sale.confirmed == 0 %}
              <span class='label label-warning'>Belum Terkonfirmasi</span>
            {% elseif sale.confirmed == 1 %}
              <span class='label label-success'>Diterima</span>
            {% elseif sale.confirmed == 2 %}
              <span class='label label-danger'>Ditolak</span>
            {% endif %}
          </td>
          {% if can("update", "sales") %}
            <td>
              {% if sale.confirmed == 0 %}
                {{
                  a("<i class='glyphicon glyphicon-ok'></i> Terima", "agree_sale", {id:sale.id}, {method:"patch", remote:"true", 
                    confirmTitle:"Konfirmasi Penjualan",
                    confirmMessage:"Apakah anda yakin untuk menyetujui penjualan berikut?"
                  }) 
                }}
                |
                {{
                  a("<i class='glyphicon glyphicon-remove'></i> Tolak", "disagree_sale", {id:sale.id}, {method:"patch", remote:"true", 
                    confirmTitle:"Konfirmasi Penjualan",
                    confirmMessage:"Apakah anda yakin untuk menolak penjualan berikut?"
                  }) 
                }}
                <br/>
                {{
                  a("<i class='glyphicon glyphicon-envelope'></i> Kirim ulang konfirmasi pembayaran", "resend_payment_confirmation", {id:sale.id}, {method:"post", remote:"true", 
                    confirmTitle:"Konfirmasi Penjualan",
                    confirmMessage:"Apakah anda yakin untuk mengirim ulang konfirmasi pembayaran dari data penjualan berikut?",
                  }) 
                }}
                <br/>
              {% endif %}
              {% if sale.payment_confirmation_image %}
                {{
                  a("<i class='glyphicon glyphicon-fullscreen'></i> Lihat bukti pembayaran", "payment_invoice", {id:sale.id}) 
                }}
              {% elseif sale.confirmed != 0 %}
                -
              {% endif %}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}