<!DOCTYPE html>
<html>
<head>
  <title>Sistem Informasi Penjualan CD/DVD</title>
  <?php
    css(array(
      'font-awesome/font-awesome.min',
      'bootstrap',
      'summernote',
      'summernote-bs3',
      'datepicker'
    ));
    js(array(
      'jquery-1.10.2.min',
      'bootstrap.min',
      'jquery.validate.min',
      'jquery.form.min',
      'bootstrap3-typeahead.min',
      'bootstrap-datepicker',
      'summernote.min',
      'app'
    )); 
  ?>
</head>
<body>
  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Tampilkan/Sembunyikan Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        {{ a("Sistem Informasi Penjualan CD/DVD", "guest#index", {}, {class:"navbar-brand"}) }}
      </div>
      {% include "shared/_menu.php" %}
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-40" style="padding: 0px 0px 100px 0px">
        {% block body %}{% endblock %}
      </div>
    </div>
  </div>
</body>
</html>