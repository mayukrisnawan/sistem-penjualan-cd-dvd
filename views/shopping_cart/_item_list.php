<div class="panel" style="">
  <div class="panel-heading" style="border-bottom:solid 1px #f9f9f9">
    <h3>
      <i class='glyphicon glyphicon-shopping-cart'></i>
      Keranjang Belanja
    </h3>

    {% if cart_items|length != 0 %}
	    <a href="{{ u('shopping_cart#clear') }}" method="get" remote="true">
	    	<i class='glyphicon glyphicon-trash'></i>
	    	Bersihkan keranjang belanja
	    </a>
    {% endif %}
  </div>
  <div class="panel-body">
  	{% if cart_items|length == 0 %}
  		<center>
  			<i>Belum ada barang yang ditambahkan.</i>
  		</center>
  	{% else %}
      {% include "shopping_cart/_item_table.php" %}
      <a href="{{ u('shopping_cart#save') }}" class='btn btn-info pull-right'>
        <i class="glyphicon glyphicon-ok"></i>
        Simpan Keranjang Belanja &gt;&gt;
      </a>
  	{% endif %}
  </div>
</div>