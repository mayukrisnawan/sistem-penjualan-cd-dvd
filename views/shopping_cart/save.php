{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Simpan Keranjang Belanja</h1>
<hr>
<p style="font-style:italic">
*Konfirmasi pembayaran akan dikirimkan melalui email
</p>
<div class='row'>
	<div class='col-md-6'>
		{% include "shopping_cart/_checkout_form.php" %}	
	</div>
</div>
{% endblock %}