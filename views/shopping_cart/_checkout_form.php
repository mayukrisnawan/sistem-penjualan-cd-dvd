<form role="form" name='sale_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="sale[id]" value="{{ sale.id }}">

  <div class="form-group">
    <label class="control-label">Alamat Email</label>
    <input name="sale[email]" type="text" class="form-control" required/>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Alamat Tinggal</label>
    <textarea name="sale[address]" class="form-control" rows="8" required></textarea>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Tinggalkan Pesan</label>
    <textarea name="sale[message]" class="form-control" rows="8"></textarea>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("sale"); ?>
</script>