<form action="" role="form">
  <table class="table table-bordered table-striped table-hover"/>
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Barang</th>
        <th>Tipe Barang</th>
        <th>Jumlah</th>
        <th>Harga per pcs</th>
        <th>Diskon</th>
        <th>Total</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {% set total_jumlah = 0 %}
      {% set total_harga = 0 %}
  		{% for cart_item in cart_items %}
        <tr>
          <td>{{ loop.index }}</td>
          <td>{{ cart_item.info.title }}</td>
          <td>{{ cart_item.info.type.name }}</td>
          <td>{{ cart_item.count }} pcs</td>
          <td>Rp {{ cart_item.info.price }}</td>
          <td>{{ cart_item.info.discount_rate() * 100 }} %</td>
          <td>Rp {{ cart_item.count * cart_item.info.price * (1-cart_item.info.discount_rate()) }}</td>
          <td>
            {{
              a("<i class='glyphicon glyphicon-trash'></i> hapus dari keranjang belanja", "delete_shopping_cart_item", {id:cart_item.info.id}, {method:"delete", remote:"true", 
                confirmTitle:"Keranjang Belanja",
                confirmMessage:"Apakah anda yakin ingin menghapus barang berikut dari keranjang belanja?",
                class:"btn_hapus"
              }) 
            }}
          </td>
        </tr>
        {% set total_jumlah = total_jumlah + cart_item.count %}
        {% set total_harga = total_harga + cart_item.info.price * cart_item.count * (1-cart_item.info.discount_rate()) %}
  		{% endfor %}
      <tr>
        <td colspan="3" align="right"><b>Total</b></td>
        <td colspan="3">{{ total_jumlah }} pcs</td>
        <td colspan="2">Rp {{ total_harga }}</td>
      </tr>
    </tbody>
  </table>
</form>