$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> data pembelian berhasil ditambahkan, {{ a('Kembali','purchases#index') }}")
           .show();
$("form[name='item_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='item_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");