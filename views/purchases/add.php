{% extends "layouts/application_layout.php" %}

{% block body %}
<script type="text/javascript">
var preffered_items = {};
</script>
<h1>Catat Pembelian Baru</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'purchases/form.php' %}
  </div>
  <div class="col-md-6" id="item_search_container">
  	{% include "items/_search_items.php" %}
  </div>
</div>
{% endblock %}