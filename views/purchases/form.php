<form role="form" name='purchase_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="purchase[id]" value="{{ purchase.id }}">

  <div class="form-group">
    <label class="control-label">Supplier</label>
    <select name="purchase[supplier_id]" class="form-control">
      {% for supplier in suppliers %}
        <option value="{{ supplier.id }}" {{ supplier.id == purchase.supplier_id ? "selected" : "" }}>{{ supplier.name }}</option>
      {% endfor %}
    </select>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Tanggal Pembelian</label>
    <div class='input-group'>
      <input name="purchase[at]" type='text' class='form-control datepicker' readonly value="<?php echo '{{ purchase.at.format("d-m-Y") }}' ? '{{ purchase.at.format("d-m-Y") }}' : date('d-m-Y'); ?>"/>
      <span class='input-group-addon'>
        <i class='glyphicon glyphicon-calendar'></i>
      </span>
    </div>
    <span class="help-block"></span>
  </div>

  <script type="text/javascript">
    var items = {};

    if (preffered_items) {
      items = preffered_items;
      $(function(){
        updateItems();
      })
    }

    function updateItems() {
      var $container = $("#purchase_items_container");
      $container.html("<b>Daftar Barang :</b><br/>");
      $container.append("<ul>");
        for (item_id in items) {
          $container.append("\
          <input type='hidden' name='purchase_item_count[" + item_id + "]' value='" + items[item_id].count + "'/>\
          <input type='hidden' name='purchase_item_price[" + item_id + "]' value='" + items[item_id].price + "'/>\
          <li class='item_container' data-item-id='" + item_id + "'>\
          " + items[item_id].type + " " + items[item_id].title + " (" +  items[item_id].count + " pcs @ Rp " + items[item_id].price  + ")\
          &nbsp;<a href='' class='btn-remove-item' data-item-id='" + item_id + "'>[x]</a>\
          </li>");
        }
      $container.append("</ul>");
      $(".btn-remove-item").click(function(){
        var item_id = $(this).attr("data-item-id");
        delete items[item_id];
        updateItems();
        return false;
      });
    }

    var search_item_callback = function(){
      $(".btn-select-item").click(function(){
        var item_id = $(this).attr("data-id");
        var title = $(this).attr("data-title");
        var type = $(this).attr("data-type");

        var count = prompt("Jumlah barang :");
        count = (new Number(count)).valueOf();
        if (isNaN(count)) return false;

        var price = prompt("Harga barang/pcs : Rp ");
        price = (new Number(price)).valueOf();
        if (isNaN(price)) return false;


        if (items[item_id] === undefined) items[item_id] = {
          title: title, 
          type: type,
          count: 0,
          price: price
        }
        items[item_id].count += count;
        updateItems();
        return false;
      });
    }
  </script>

  <div id="purchase_items_container">
  </div>

  <hr>

  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("purchase"); ?>
</script>