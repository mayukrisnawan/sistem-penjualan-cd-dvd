{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Detail Pembelian</h1>
<hr/>
<h4>Tanggal : {{ purchase.at.format('d-M-Y') }}</h4>
<h4>Supplier : {{ purchase.supplier.name }}</h4>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Barang</th>
        <th>Tipe Barang</th>
        <th>Jumlah</th>
        <th>Harga per pcs</th>
        <th>Total</th>
      </tr>
    </thead>
    {% set total_harga = 0 %}
    {% set total_jumlah = 0 %}
    <tbody>
      {% for purchase_item in purchase.purchase_items %}
        <tr>
          <td>{{ loop.index }}</td>
          <td>{{ purchase_item.item.title }}</td>
          <td>{{ purchase_item.item.type.name }}</td>
	        <td>{{ purchase_item.count }}</td>
	        <td>Rp {{ purchase_item.price }}</td>
	        <td>Rp {{ purchase_item.count * purchase_item.price }}</td>
	        {% set total_jumlah = total_jumlah + purchase_item.count %}
	        {% set total_harga = total_harga + purchase_item.count * purchase_item.price %}
        </tr>
      {% endfor %}
      <tr>
      	<td colspan="3" align="right"><b>Total</b></td>
      	<td colspan="2">{{ total_jumlah }} pcs</td>
      	<td>Rp {{ total_harga }}</td>
      </tr>
    </tbody>
  </table>
</div>
{% endblock %}