<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Tanggal Pembelian</th>
        <th>Supplier</th>
        <th>Total</th>
        {% if can('read', 'purchase_items') or can('delete', 'purchases') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('read', 'purchase_items') or can('delete', 'purchases') ? 6 : 5 }}">
            <center>
              <i>Belum ada pembelian yang dicatat</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for purchase in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ purchase.at.format("d-m-Y") }}
          </td>
          <td>
            {{ purchase.supplier.name }}
          </td>
          <td>
            {% set total = 0 %}
            {% for purchase_item in purchase.purchase_items %}
              {% set total = total + purchase_item.count * purchase_item.price %}
            {% endfor %}
            Rp {{ total }}
          </td>

          {% if can('read', 'purchase_items') or can('delete', 'purchases') %}
            <td>
              {% if can('delete', 'purchases') %}
                {{
                  a("Hapus", "purchases#destroy", {id:purchase.id}, {method:"delete", remote:"true", 
                    confirmTitle:"Data Pembelian",
                    confirmMessage:"Apakah anda yakin ingin menghapus data pembelian berikut?",
                    class:"btn_hapus"
                  })
                }}
              {% endif %}
              {% if can('read', 'purchase_items') and can('delete', 'purchases') %}
              |
              {% endif %}
              {% if can('read', 'purchase_items') %}
                {{
                  a("Detail Pembelian", "purchases#show", {id:purchase.id})
                }}
              {% endif %}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}