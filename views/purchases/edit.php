{% extends "layouts/application_layout.php" %}

{% block body %}
<script type="text/javascript">
	var preffered_items = {};
	{% for purchase_item in purchase_items %}
		preffered_items[{{ purchase_item.item_id }}] = {
			title: '{{ purchase_item.item.title }}',
			type: '{{ purchase_item.item.type.name }}',
			count: {{ purchase_item.count }}
		}
	{% endfor %}
</script>
<h1>Edit Pembelian</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'purchases/form.php' %}
  </div>
  <div class="col-md-6" id="item_search_container">
  	{% include "items/_search_items.php" %}
  </div>
</div>
{% endblock %}