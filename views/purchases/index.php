{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Pembelian</h1>
<hr/>
{% if can('create', 'purchases') %}
	<form class="form-inline" style="margin : 10px auto">
	  <div class="form-group">
	    {{ a("<span class='glyphicon glyphicon-plus'></span> Catat Pembelian Baru", "purchases#add", {}, {class:"btn btn-success form-control"}) }}
	  </div>
	</form>
{% endif %}
{% include "purchases/_table.php" %}
{% endblock %}