{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Ganti Password</h1>
<div class="row">
  <div class="col-md-6">
    <form role="form" id="change_password_form" action="{{ u('change_password') }}" method="post">
    	<div class="form-group">
    		<label>Password Lama</label>
    		<input type="text" class='form-control' name="old_password">
    	</div>
    	<div class="form-group">
    		<label>Password Baru</label>
    		<input type="text" class='form-control' name="new_password">
    	</div>
    	<div class="form-group">
    		<label>Ketik Ulang Password baru</label>
    		<input type="text" class='form-control' name="new_password_retype">
    	</div>
    	<div class="form-group">
    		<input type="submit" class="btn btn-success" value="Simpan">
    	</div>
    </form>
  </div>
</div>
<script type="text/javascript">
	$("#change_password_form").ajaxForm({
        dataType:"script"
    });
</script>
{% endblock %}