$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> pengguna berhasil ditambahkan, {{ a('Kembali','users#index') }}")
           .show();
$("form[name='user_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='user_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");