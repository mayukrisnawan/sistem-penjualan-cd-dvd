{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Pengguna</h1>
<hr/>
{% if can('create', 'users') %}
  <form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
      {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah pengguna baru", "users#add", {}, {class:"btn btn-success form-control"}) }}
    </div>
  </form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Username</th>
        <th>Password</th>
        <th>Jabatan Pengguna</th>
        {% if can('update', 'users') or can('delete', 'users') or can('read', 'user_roles') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('update', 'users') or can('delete', 'users') or can('read', 'user_roles') ? 5 : 4 }}">
            <center>
              <i>Belum ada pengguna</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for user in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ user.username }}
          </td>
          <td>
            {% if md5(user.username) == user.password %}
              {{ user.username }}
            {% else %}
              {% if can('manage', 'users') %}
                {{ 
                  a("Reset password", "reset_password", {id:user.id}, {method:"patch", remote:"true", 
                    confirmTitle:"Reset Password",
                    confirmMessage:"Apakah anda yakin ingin mangatur ulang kata sandi pengguna berikut?"
                  }) 
                }}
              {% else %}
                Sudah dirubah
              {% endif %}
            {% endif %}
          </td>
          <td>
            {% for index,role in user.roles %}
              {{ role.name }}
              {{ index != user.roles.length-1 ? '' : ', ' }}
            {% endfor %}
          </td>
          {% if can('update', 'users') or can('delete', 'users') or can('read', 'user_roles') %}
            <td>
              {% if can('update', 'users') %}
                {{ 
                  a("Edit Username", "users#edit", {id:user.id}) 
                }}
              {% endif %}
              {% if can('update', 'users') and can('delete', 'users') %}
              |
              {% endif %}
              {% if can('delete', 'users') %}
              {{
                a("Hapus", "users#destroy", {id:user.id}, {method:"delete", remote:"true", 
                  confirmTitle:"Data Pengguna",
                  confirmMessage:"Apakah anda yakin ingin menghapus pengguna berikut?",
                  class:"btn_hapus"
                }) 
              }}
              {% endif %}
              {% if can('delete', 'users') and can('read', 'user_roles') %}
              |
              {% endif %}
              {% if can('read', 'user_roles') %}
              {{ 
                a("Edit Jabatan Pengguna", "user_user_roles#index", {user_id:user.id}) 
              }}
              {% endif %}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}