{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Pengguna</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'users/form.php' %}
  </div>
</div>
{% endblock %}