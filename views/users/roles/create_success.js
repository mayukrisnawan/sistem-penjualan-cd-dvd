$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> jabatan pengguna berhasil ditambahkan, {{ a('Kembali','user_user_roles#index', { user_id:user.id }) }}")
           .show();
$("form[name='user_role_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='user_role_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");