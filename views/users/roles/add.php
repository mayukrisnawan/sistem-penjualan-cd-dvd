{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Jabatan Pengguna >> {{ user.username }}</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'users/roles/form.php' %}
  </div>
</div>
{% endblock %}