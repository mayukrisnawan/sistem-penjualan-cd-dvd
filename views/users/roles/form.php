<form role="form" name='user_role_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="user_role[user_id]" value="{{ user.id }}">
  <div class="form-group">
    <label class="control-label">Pilih Jabatan</label>
    <select class='form-control' name='user_role[role_id]'>
      <option value="">--Pilih jabatan pengguna--</option>
      {% for role in roles %}
        <option value="{{ role.id }}">{{ role.name }}</option>
      {% endfor %}
    </select>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" id="btn_simpan" class="btn btn-success" value="Simpan">
  </div>
</form>

<script type="text/javascript">
  <?php jQueryValidate("user_role") ?>;
</script>