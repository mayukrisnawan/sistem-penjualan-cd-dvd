{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Jabatan Pengguna >> {{ user.username }}</h1>
<hr/>
{% if can('create', 'user_roles') %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah jabatan baru", "user_user_roles#add", {user_id:user.id}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama Jabatan</th>
        {% if can('delete', 'user_roles') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if user_roles|length == 0 %}
        <tr>
          <td colspan="{{ can('delete', 'user_roles') ? 3 : 2 }}">
            <center>
              <i>Belum ada jabatan pengguna yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for user_role in user_roles %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ user_role.role.name }}
          </td>
          {% if can('delete', 'user_roles') %}
            <td>
              {{
                a("Hapus", "user_roles#destroy", {id:user_role.id}, {method:"delete", remote:"true", 
                  confirmTitle:"Jabatan Pengguna",
                  confirmMessage:"Apakah anda yakin ingin membatalkan jabatan pengguna berikut?",
                  class:"btn_hapus"
                }) 
              }}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}