<form role="form" name='supplier_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="supplier[id]" value="{{ supplier.id }}">

  <div class="form-group">
    <label class="control-label">Nama Supplier</label>
    <input name="supplier[name]" type="text" class="form-control" value="{{ supplier.name }}"/>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">No Telp. Supplier</label>
    <div class="input-group">
      <input name="supplier[phone]" type="text" class="form-control" value="{{ supplier.phone }}"/>
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-phone"></i>
      </span>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Alamat Email Supplier</label>
    <div class="input-group">
      <input name="supplier[email]" type="text" class="form-control" value="{{ supplier.email }}"/>
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-envelope"></i>
      </span>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("supplier"); ?>
</script>