{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Supplier Baru</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'suppliers/form.php' %}
  </div>
</div>
{% endblock %}