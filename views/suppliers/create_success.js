$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> supplier berhasil ditambahkan, {{ a('Kembali','suppliers#index') }}")
           .show();
$("form[name='supplier_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='supplier_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");