{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Supplier</h1>
<hr/>
{% if can('create', 'suppliers') %}
  <form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
      {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah supplier baru", "suppliers#add", {}, {class:"btn btn-success form-control"}) }}
    </div>
  </form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama</th>
        <th>No Telp</th>
        <th>Alamat Email</th>
        {% if can('update', 'suppliers') or can('delete', 'suppliers') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{ can('update', 'suppliers') or can('delete', 'suppliers') ? 5 : 4 }}">
            <center>
              <i>Belum ada supplier yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for supplier in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ supplier.name }}
          </td>
          <td>
            {{ supplier.phone }}
          </td>
          <td>
            {{ supplier.email }}
          </td>
          {% if can('update', 'suppliers') or can('delete', 'suppliers') %}
            <td>
              {% if can('update', 'suppliers') %}
                {{ 
                  a("Edit", "suppliers#edit", {id:supplier.id}) 
                }}
              {% endif %}
              {% if can('update', 'suppliers') and can('delete', 'suppliers') %}
              |
              {% endif %}
              {% if can('delete', 'suppliers') %}
                {{
                  a("Hapus", "suppliers#destroy", {id:supplier.id}, {method:"delete", remote:"true", 
                    confirmTitle:"Data Supplier",
                    confirmMessage:"Apakah anda yakin ingin menghapus supplier berikut?",
                    class:"btn_hapus"
                  }) 
                }}
              {% endif %}
            </td>
          {% endif %}
        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}