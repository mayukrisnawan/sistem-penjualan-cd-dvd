{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Daftar Tipe Barang</h1>
<hr/>
{% if can('create', 'types') %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah tipe barang baru", "types#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th width="1%">No</th>
        <th>Nama</th>
        {% if can('update', 'types') or can('delete', 'types') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
        <tr>
          <td colspan="{{  can('update', 'types') or can('delete', 'types') ? 3 : 2 }}">
            <center>
              <i>Belum ada tipe barang yang ditambahkan</i>
            </center>
          </td>
        </tr>
      {% endif %}

      {% for type in pagination.records %}
        <tr>
          <td>{{ loop.index + pagination.offset }}</td>
          <td>
            {{ type.name }}
          </td>
          {% if can('update', 'types') or can('delete', 'types') %}
            <td>
              {% if can('update', 'types') %}
                {{ 
                  a("Edit", "types#edit", {id:type.id}) 
                }}
              {% endif %}
              {% if can('update', 'types') and can('delete', 'types') %}
              |
              {% endif %}
              {% if can('delete', 'types') %}
              {{
                a("Hapus", "types#destroy", {id:type.id}, {method:"delete", remote:"true", 
                  confirmTitle:"Tipe Barang",
                  confirmMessage:"Apakah anda yakin ingin menghapus tipe barang berikut?",
                  class:"btn_hapus"
                }) 
              }}
              {% endif %}
            </td>
          {% endif %}

        </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}