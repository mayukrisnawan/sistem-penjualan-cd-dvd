<form role="form" name='type_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="type[id]" value="{{ type.id }}">
  <div class="form-group">
    <label class="control-label">Nama Tipe Barang</label>
    <input name="type[name]" type="text" class="form-control" value="{{ type.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("type"); ?>
</script>