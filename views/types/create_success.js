$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> tipe barang berhasil ditambahkan, {{ a('Kembali','types#index') }}")
           .show();
$("form[name='type_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='type_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");