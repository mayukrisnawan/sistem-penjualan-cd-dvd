{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Tambah Tipe Barang Baru</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'types/form.php' %}
  </div>
</div>
{% endblock %}