{% extends "layouts/application_layout.php" %}

{% block body %}
<h1>Edit Tipe Barang >> {{ type.name }}</h1>
<div class="row">
  <div class="col-md-6">
    {% include 'types/form.php' %}
  </div>
</div>
{% endblock %}