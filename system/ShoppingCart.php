<?php

class ShoppingCart {
	public static function all() {
		if (!isset($_SESSION["cart"])) return array();
		$items = array();
		foreach ($_SESSION["cart"] as $item_id => $count) {
			try {
				$item = Item::find($item_id);
			} catch (Exception $e) {
				continue;
			}
			$items[] = array(
				"info" => $item,
				"count" => $count
			);
		}
		return $items;
	}

	public static function add($item_id, $count) {
		if ($count == 0) return false;
		if (!isset($_SESSION["cart"])) $_SESSION["cart"] = array();
		if (isset($_SESSION["cart"][$item_id])) {
			$_SESSION["cart"][$item_id] += $count;
		} else {
			$_SESSION["cart"][$item_id] = $count;
		}
	}

	public static function set($item_id, $count) {
		if ($count == 0) return false;
		if (!isset($_SESSION["cart"])) $_SESSION["cart"] = array();
		$_SESSION["cart"][$item_id] = $count;
	}

	public static function deleteItem($item_id) {
		unset($_SESSION["cart"][$item_id]);
	}


	public static function flush() {
		return $_SESSION["cart"] = array();
	}

	public static function itemCount($item_id) {
		return isset($_SESSION["cart"][$item_id]) ? $_SESSION["cart"][$item_id] : 0;
	}
}