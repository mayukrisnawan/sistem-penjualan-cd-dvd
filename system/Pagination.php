<?php

require_once "system/vendors/pagination-master/paginator.php";

class Pagination extends Paginator {
  public function limit() {
    return $this->_perPage;
  }

  public function offset() {
    return ($this->_page-1) * $this->_perPage;
  }

  public function page() {
    return $this->_page;
  }
}