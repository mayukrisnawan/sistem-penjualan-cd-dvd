<?php

class MetaData {
  private $data = array();
  public function __set($name, $value) {
    $this->data[$name] = $value;
  }
  public function __get($name) {
    return isset($this->data[$name]) ? $this->data[$name] : null;
  }
  public function all() {
    return $this->data;
  }
  public function push($name, $value) {
    if (isset($this->data[$name])) {
      $this->data[$name][] = $value;
    } else return false;
  }
  public function replace($name, $value) {
    if (isset($this->data[$name])) {
      $this->data[$name] = $value;
    } else return false;
  }
  public static function plain($instance) {
    if ($instance instanceof MetaData) {
      $result = $instance->all();
      foreach ($result as $key => $value) {
        $result[$key] = MetaData::plain($value);
      }
      return $result;
    } else {
      return $instance;
    } 
  }
}