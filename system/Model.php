<?php

require_once "system/vendors/php-activerecord/ActiveRecord.php";
require_once 'system/vendors/php-activerecord/lib/Validations.php';
require_once "system/Pagination.php";

ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory('models');
    $connections = App::config("connections");
    $connection_strings = array();
    foreach ($connections as $mode => $connection) {
      $loginInfo = $connection['username'];
      if (isset($connection['password'])) {
        $loginInfo .= ":" . $connection['password']; 
      }
      $connection_string = "mysql://$loginInfo"."@".$connection["host"]."/".$connection["database"];
      $connection_strings[$mode] = $connection_string;
    }
    $cfg->set_connections($connection_strings);
    $cfg->set_default_connection(App::config("current_mode"));
});

class Model extends ActiveRecord\Model {
  protected static $itemPerPage = 10;
  protected static $paginator = null;

  public static function paginate(array $options=array(), $key="_page") {
    static::$paginator = new Pagination(static::$itemPerPage, $key);
    $countOptions = $options;
    $pk = static::$primary_key ? static::$primary_key : "id";
    $countOptions["select"] = "count(".static::table_name().".$pk) AS cnt";
    $countOptions["include"] = null;
    static::$paginator->set_total(static::first($countOptions)->cnt);

    if (!isset($options["limit"])) $options["limit"] = static::$paginator->limit();
    if (!isset($options["offset"])) $options["offset"] = static::$paginator->offset();

    $result = new MetaData();
    $result->records = static::find("all", $options);
    $result->links = static::$paginator->page_links();
    $result->limit = static::$paginator->limit();
    $result->offset = static::$paginator->offset();
    return $result;
  }

  public static function validationRules() {
    $className = get_called_class();
    $validator = new ActiveRecord\Validations(new $className);
    return $validator->rules();
  }

  public function toArray(array $options=array())
  {
    return $this->serialize('Array', $options);
  }

  public static function plain(array $models=array()) {
    $result = array();
    foreach ($models as $model) {
      $result[] = $model->toArray();
    }
    return $result;
  }

  public static function json(array $models=array()) {
    echo json_encode(static::plain($models));
  }

  public static function options(array $models, $content_key, $value_key="id") {
    $result = array(
      "content_key"=>$content_key,
      "value_key"=>$value_key,
      "data"=>array()
    );
    foreach ($models as $model) {
      $result["data"][] = array(
        $value_key=>$model->$value_key,
        $content_key=>$model->$content_key
      );
    }
    return json_encode($result);
  }
}