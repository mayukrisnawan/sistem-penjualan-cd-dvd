<?php

class DB {
  public static function connect() {
    $config = include("config/application.php");
    $connections = $config["connections"];
    $mode = $config["current_mode"];
    $connection = mysql_connect(
      $connections[$mode]["host"], 
      $connections[$mode]["username"],
      $connections[$mode]["password"]
    );
    mysql_select_db($connections[$mode]["database"], $connection);
    return $connection;
  }
}