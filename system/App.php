<?php
session_start();

// Pear Mail Library
require_once "Mail.php";

require_once "system/helpers/common.php";
require_once "system/DB.php";
require_once "system/Model.php";
require_once "system/Router.php";
require_once "system/TwitterBootstrap.php";
require_once "system/helpers/form.php";
require_once "system/helpers/auth.php";
require_once "system/Controller.php";
require_once "system/ShoppingCart.php";

class App {
  public static $configs = null;

  public static function run() {  
    // Render resource yang bersifat public
    $publicRequest = "\\".dirname($_SERVER["PHP_SELF"]) . "\/public\/";
    if (preg_match("/^$publicRequest/", $_SERVER["REQUEST_URI"])) {
      $path = $_SERVER["REQUEST_URI"];
      $path = str_replace(dirname($_SERVER["PHP_SELF"]), "", $path);
      $path = ltrim($path, "/");
      App::renderPublic($path);
      return;
    }

    // Menjalankan router
    require_once "config/routes.php";
    Router::run();
    if (!Router::target()) {
      App::notFound();
      return;
    }

    // Menentukan handler request
    $target = explode("#", Router::target());
    $controller = $target[0];
    $method = isset($target[1]) ? $target[1] : "index";

    // Menentukan hak akses default
    // $default_user_level = App::config("default_user_level");
    // UM::requiredLevels($default_user_level);
    App::render($controller, $method, params());
  }

  public static function render($controllerName, $methodName, $params) {
    $words = explode("_", $controllerName);
    $className = "";
    for ($i=0; $i < count($words); $i++) { 
      $className .= ucfirst($words[$i]);
    }
    $className = $className . "Controller";

    $path = "controllers/" . $className . ".php";
    if (!file_exists($path)) {
      throw new Exception("Controller $className tidak ditemukan");
      exit();
    }

    $ctrlName = $className;
    if ($methodName == "__construct" || $methodName == "__destruct") App::notFound();

    try {
      $ctrl = new ReflectionMethod($ctrlName, $methodName);
    } catch (ReflectionException $e) {
      throw new Exception("Controller $className dengan method $methodName tidak ditemukan");
      exit();
    } 
    
    if ($ctrl->getNumberOfRequiredParameters() > count($params)) {
      App::notFound();
      exit();
    }

    $currentController = new $ctrlName;
    $ctrl->invokeArgs($currentController, $params);
  }

  public static function renderPublic($path) {
    $path = urldecode($path);
    $path = explode("?", $path);
    $path = $path[0];
    TwitterBootstrap::initLessCompiler();
    TwitterBootstrap::checkedCompile("public/css/bootstrap.css.less", $path);

    if (!file_exists($path) || is_dir($path)) {
      $path = $path . ".less";
      if (!file_exists($path)) {
        App::notFound();
        return;
      }
    }

    $contentType = getContentType($path);
    if ($contentType == "php") {
      include $path;
      return;
    }

    // Menghitung waktu untuk kalkulasi cache
    $lastModified = filemtime($path);
    $dayOfWeek = gmdate("l", $lastModified);
    $dayOfWeek = substr($dayOfWeek, 0, 3);
    $lastModified = gmdate("d M Y H:i:s", $lastModified);
    $lastModified = "$dayOfWeek, $lastModified GMT";

    $exps = filemtime($path)+31536000;
    $dayOfWeekExp = gmdate("l", $exps);
    $dayOfWeekExp = substr($dayOfWeekExp, 0, 3);
    $exps = gmdate("d M Y H:i:s", $exps);
    $exps = "$dayOfWeekExp, $exps GMT";

    // Setting header untuk response manual
    header("Content-type: $contentType");
    header("Cache-Control: public, max-age=31536000");
    header("Last Modified: $lastModified");
    header("Expires: $exps");
    header_remove("Pragma");

    if (TwitterBootstrap::requestedBy($path)) {
      $contents = TwitterBootstrap::fetch();
    } else if (fileExtension($path) == "less") {
      $parser = new Less_Parser();
      $parser->parseFile($path);
      $contents = $parser->getCss();
    } else {
      $contents = file_get_contents($path);
    }
    echo $contents;
  }

  public static function sendFile($contents, $filename) {
    $contentType = getContentType($filename);
    header('Content-Type: $contentType');
    header("Content-Disposition: attachment;filename='$filename'");
    echo $contents;
  }

  public static function stop() {
    exit();
  }

  public static function notFound() {
    header("HTTP/1.1 404 Not Found");
    include "public/not_found.php";
    exit();
  }

  public static function config($name) {
    if (!App::$configs) App::$configs = include("config/application.php");
    return isset(App::$configs[$name]) ? App::$configs[$name] : false;
  }
}