<?php

function getBasePath() {
  return "http://". $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
}

function url($url) {
  return getBasePath() . "/" . $url;
}

function css($css) {
  if (is_array($css)) {
    foreach ($css as $c) {
     css($c);
    }
  } else {
    echo "<link rel='stylesheet' type='text/css' href='". url("public/css/".$css) .".css'/>\n";
  }
}

function js($js) {
  if (is_array($js)) {
    foreach ($js as $j) {
     js($j);
    }
  } else {
    echo "<script type='text/javascript' src='". url("public/js/".$js) .".js'></script>\n";
  }
}

function u($routeName, array $params=array()) {
  $url = Router::generate($routeName, $params);
  if (isset($params["_adder"])) {
    $url .= $params["_adder"];
  }
  return $url;
}

function eu($routeName, array $params=array()) {
  echo u($routeName, $params);
}

function fileExtension($path) {
  $path_parts = pathinfo($path);
  return $path_parts['extension'];
}

function getContentType($path) {
  $mimetypes = include("config/content_type.php");
  $mimetypes["php"] = "php";
  $ext = fileExtension($path);
  if (array_key_exists($ext, $mimetypes)) {
      $mime = $mimetypes[$ext];
  } else {
      $mime = 'application/octet-stream';
  }
  return $mime;
}

function params($name=null) {
  $params = Router::params();
  if ($name) {
    return isset($params[$name]) ? $params[$name] : false;
  } else {
    return $params;
  }
}