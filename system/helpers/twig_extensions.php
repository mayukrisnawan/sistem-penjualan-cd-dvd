<?php

return function($engine){
  // url function
  $u = new Twig_SimpleFunction('u', function($name, array $params=array()){
    return u($name, $params);
  });

  // anchor function
  $a = new Twig_SimpleFunction('a', function($label, $routeName, array $routeParams=array(), array $anchorAttributes=array()){
    try {
      $url = u($routeName, $routeParams);
    } catch (Exception $e) {
      $url = $routeName;
    }
    $attr = "";
    foreach ($anchorAttributes as $key => $value) {
      $attr .= " " . $key . "='" . $value . "'";
    }
    return "<a href='$url'".$attr.">$label</a>";
  }, array(
    'is_safe'=>array('html')
  ));

  // img function
  $img = new Twig_SimpleFunction('img', function($name, array $attributes=array()){
    $url = url("public/img/$name");
    $attr = "";
    foreach ($attributes as $key => $value) {
      $attr .= " " . $key . "='" . $value . "'";
    }
    return "<img src='$url'".$attr."/>";
  }, array(
    'is_safe'=>array('html')
  ));

  // var_dump function
  $var_dump = new Twig_SimpleFunction('var_dump', function($value){
    return var_dump($value);
  }, array(
    'is_safe'=>array('html')
  ));

  // md5 function
  $md5 = new Twig_SimpleFunction('md5', function($input){
    return md5($input);
  });

  // current_user function
  $current_user = new Twig_SimpleFunction('current_user', function(){
    return current_user();
  });

  // can function
  $can = new Twig_SimpleFunction('can', function($modifier, $resourceName){
    return can($modifier, $resourceName);
  });

  // restrict function
  $restrict = new Twig_SimpleFunction('restrict', function($modifier, $resourceName){
    return restrict($modifier, $resourceName);
  });

  // extending the engine
  $engine->addFunction($u);
  $engine->addFunction($a);
  $engine->addFunction($img);
  $engine->addFunction($var_dump);
  $engine->addFunction($md5);
  $engine->addFunction($current_user);
  $engine->addFunction($can);
  $engine->addFunction($restrict);
}

?>