<?php

function current_user(){
  return isset($_SESSION['user_id']) ? User::find($_SESSION['user_id']) : false;
}

function can($modifier, $resourceName){
	switch ($modifier) {
		case 'manage':
			$code = "c = 1 AND r = 1 AND u = 1 AND d = 1";
			break;
		case 'create':
			$code = "c = 1";
			break;
		case 'read':
			$code = "r = 1";
			break;
		case 'update':
			$code = "u = 1";
			break;
		case 'delete':
			$code = "d = 1";
			break;
		default:
			$code = "c = 1 AND r = 1 AND u = 1 AND d = 1";
			break;
	}

	$user = current_user();
	if (!$user) return false;
	$raf = RoleApplicationFeature::first(array(
		"conditions" => array(
			"role_id IN (SELECT role_id FROM user_roles WHERE user_id = ?)
			AND
			application_feature_id IN (SELECT id FROM application_features WHERE application_features.key = ?)
			AND $code",
			$user->id, $resourceName
		)
	));
	return $raf ? true : false;
}

function restrict($modifier, $resourceName) {
	if (!can($modifier, $resourceName)) App::notFound();
}